#pragma once
#include "includes.h"
#include "ShaderClass.h"
#include "GameObject.h"
//#include "Scene.h"  //hna lw di hna brdo tb2a circular include , fa 7atetha extern class we 7atetha fl cpp

////////////////////////////// IMPORTANT NOTE ///////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// COPY this class and change it's name and add parameters to create another kinds of gameObjects /////////
//// this is the bare minimum to show how to do it and provide a reference to copy from. ////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern class Scene;

class Sprite : public GameObject
{

private:
	//INHERITS:
	// - transform
	// - texture
	// - motionLib
	// - *myshader
	// - virtual void render()
	// - void(*Update)(GameObject * me); 
	// - void(*Start)(GameObject * me);
	// - *Scene
	// - windowheight, width

	GLFWwindow * window;
public:

	//constructor (refPosition, imageFile, shader, window, StartFunction, UpdateFunction, Scene)
	Sprite(glm::vec2 TransPos, const char * file, ShaderClass * shader, GLFWwindow * wind, void(*start)(GameObject * me), void(*upate)(GameObject* me), Scene * scene);
	
	void setOnUpdate(void (*)(GameObject *)); //takes a pointer to function of type of Update function 
	void setOnStart(void(*)(GameObject *)); //takes a pointer to function of type of Start function

	//here create another customUpdate function - which takes the argument with the same kind of the class
	//which i will set and call manually 
	// void(*Update)(Sprite * me);

	//INHERITS virtual function render
	//render the sprite using its current shader
	void render();

	//destructor 
	~Sprite();
};

