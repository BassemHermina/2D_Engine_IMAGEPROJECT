#pragma once
#include "includes.h"
#include "EventSystem.h"
#include "cvHough.h" //to include the hitData struct
class cvFrameDiff
{
private:
	cv::VideoCapture *cap;
	cv::Mat currentFrame;
	cv::Mat prevFrame;
	cv::Mat workingFrame;
	std::vector <contour> Contours;
	int howmanycenters;
	Hands currentFrameHands;
	Hands prevFrameHands; //their contours are deleted;
	std::vector <Hand> suggestedHandPoints;
	bool no2GoodHands = true;
	std::vector <cv::Point> noiseHandlingPoint;
	cv::Ptr<cv::BackgroundSubtractorMOG2> bgm;

	cv::Point findCenter(std::vector <cv::Point> cont);
	int findSmallestDistance(cv::Point);


	std::vector<std::vector <hit_data>>hits;
	EventSystem * ev;

	Hands lazyNazumiHands_current;
	Hands lazyNazumiHands_prev;

public:
	bool videoEnd; //check for this in the loop 
	void wasteFrames(int f); //wastes initial @f frames from the camera/video
	cvFrameDiff(EventSystem * );

	int hitCount = 0;

	void initializeCam();
	void releaseCam();
	void lazyNazumi();
	void NaiveHitCheck();
	void sendHitToGUI(float vel, glm::vec2 pos);

	/*
	* @Step			0
	* @Name			get frame
	* @Params		Show enum: SHOW, NOSHOW
	* @Behaviour	- copies the data in @currentFrame into @prevFrame
	*				- gets a frame from the camera into @currentFrame
	*				- in the main loop, check if it returns false to break from the loop
	*				  as it reports the end of the cap stream
	*/
	bool getFrame(Show t);

	/*
	* @Step			1
	* @Name			frame difference
	* @Params		Show enum: SHOW, NOSHOW
	* @Behaviour	calculates the difference between @currentFrame and @prevFrame
	*				into @workingFrame
	*/
	void frameDifference(Show t);

	/*
	* @Step			2
	* @Name			convert Color
	* @Params		Show enum: SHOW, NOSHOW
	* @Behaviour	converts @workingFrame color from RGB to Gray
	*/
	void convertColor(Show t);

	/*
	* @Step			3
	* @Name			threshold
	* @Params		int value1, int value2, Show enum: SHOW, NOSHOW
	* @Behaviour	thresholds @workingFrame to binary Image
	*				using cv threshold function within range val1 and val2
	*/
	void threshold(int val1, int val2, Show t);

	/*
	* @Step			4
	* @Name			median
	* @Params		int matrix width, Show enum: SHOW, NOSHOW
	* @Behaviour	apply median filter to @workingFrame with element matrix width
	*/
	void median(int matWidth, Show t);

	/*
	* @Step			5
	* @Name			erode
	* @Params		Show enum: SHOW, NOSHOW
	* @Behaviour	apply erosion filter to @workingFrame with element matrix width
	*/
	void erode(int matWidth, Show t);

	/*
	* @Step			6
	* @Name			find contours
	* @Params		Show enum: SHOW, NOSHOW
	* @Behaviour	uses the cv::findContours function to find contours arround the
	*				white pixels in @workingFrame and reports them back in @Contours vector
	*/
	void findContours(Show t);

	/*
	* @Step			7
	* @Name			suggest hands
	* @Params		None
	* @Behaviour	uses the @howmanyhands as the k of k-means (number of centroids)
	*				to find the cluster centers and report them into @suggestedHandPoints vector
	*				uses enhancements steps (depending on the contour) to fix the centroids:
	*				- uses @calculateContourCenter_weighted()
	*				- uses @shiftHandCenter_AccordingToClusterPoisition()
	*/
	void suggestHands(Show t);

	/*
	* @Step			8
	* @Name			vote for 2 good hands
	* @Params		None
	* @Behaviour	uses the @suggestedHandPoints vector with the @prevFrameHands positions
	*				to calculate the distances between every pair and choose the best two
	*				points from the @suggestedHandPoints and put them into @currentFrameHands
	*				and sets @no2goodhands to false if it found 2 good matches
	*/
	void voteFor2GoodHands(Show t);

	/*
	* @Step			9
	* @Name			solve hands noise using incremented K
	* @Params		None
	* @Behaviour	increments @howmanycenters and re-calls @suggestHands and @voteFor2GoodHands
	*				to re-find the k means of the contours and choose the best 2 match of them
	*				used insides a loop untill @no2GoodHands is false
	*/
	void solveHandsNoise_usingIncrementedK(Show t);


	/*
	* @Step			10
	* @Name			re-enhance hands
	* @Params		None
	* @Behaviour	enhances hands positions based on history (of good points)
	*				-TODO: uses @shiftHandCenter_AccordingToMovementDirection() [uses history of good points and contours vector]
	*				-TODO: uses @shiftHandCenter_AccordingToKalmanFilterGuess() [uses history of good points only]
	*				- uses @naiveXSmoothing() [uses history of good points]
	*/
	void reEnhanceHands();


	/*
	* @Step			None
	* @Name			calculate clusters centers
	* @Params		None
	* @Behaviour	finds @contoursNearMe for every @suggestedHandPoints, then calculates the new
	*				centers of the @suggestedHandPoints based on their - used by the function that calculates kmeans
	*				inside @suggestHands()
	*/
	void calculateClustersCenters();

	//used by @suggestHands()
	void calculateContoursCenter_weighted();
	//used by @suggestHands()
	void shiftHandCenter_AccordingToClusterPoisition();

	void smoothWindow();

	~cvFrameDiff();
};

