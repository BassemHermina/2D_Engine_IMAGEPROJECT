#include "cvFrameDiff.h"

using namespace cv;
using namespace std;

/*
* @Name        Default Constructor
* @Params      None
* @Behaviour   Instintiates the VideoCapture object - and instantiate a frame to @currentFrame
*/
cvFrameDiff::cvFrameDiff(EventSystem * e)
{
	//cap = new VideoCapture("testcap4.avi");
	//initialize last frame hands to zero position 
	prevFrameHands.hand1.center = Point(100, 100);
	prevFrameHands.hand2.center = Point(250, 200);
	currentFrameHands.hand1.center = Point(100, 100);
	currentFrameHands.hand2.center = Point(250, 200);
	lazyNazumiHands_current.hand1.center = Point(0, 0);
	lazyNazumiHands_current.hand2.center = Point(0, 0);
	lazyNazumiHands_prev.hand1.center = Point(0, 0);
	lazyNazumiHands_prev.hand2.center = Point(0, 0);
	//hacks
	this->ev = e;
	std::vector <hit_data> dummy3;
	std::vector <hit_data> dummy4;
	hits.push_back(dummy3);
	hits.push_back(dummy3);
}

void cvFrameDiff::initializeCam(){
	cap = new VideoCapture(0);
	cap->set(CAP_PROP_FRAME_WIDTH, 320);
	cap->set(CAP_PROP_FRAME_HEIGHT, 240);
	cap->set(CAP_PROP_FPS, 120);

	if (!cap->isOpened()){
		cout << "Error opening video stream or file" << endl;
		exit(-1);
	}
	(*cap) >> currentFrame;
}

void cvFrameDiff::releaseCam(){
	cv::destroyAllWindows();
	if (cap)
		cap->release();
}

void cvFrameDiff::sendHitToGUI(float vel, glm::vec2 pos){
	ev->simulateHit(vel, pos);
}

void cvFrameDiff::wasteFrames(int f){
	while (f > 0){
		(*cap) >> currentFrame;
		if (waitKey(9) >= 0); //delay 9 millis as the camera gets a frame every 8.333 millis
		f--;
	}
}

bool cvFrameDiff::getFrame(Show t){
	// initialize class data 
	currentFrame.copyTo(prevFrame);
	prevFrameHands.hand1 = Hand(currentFrameHands.hand1.center);
	prevFrameHands.hand2 = Hand(currentFrameHands.hand2.center);
	no2GoodHands = true;
	Contours.clear();
	suggestedHandPoints.clear();
	howmanycenters = 2;

	(*cap) >> currentFrame;
	flip(currentFrame, currentFrame, 1); //mirror

	if (currentFrame.empty())
		return false;
	if (t == SHOW) imshow("getFrame->currentFrame", currentFrame);

	return true;
}

void cvFrameDiff::frameDifference(Show t){
	absdiff(currentFrame, prevFrame, workingFrame);
	if (t == SHOW) imshow("frameDifference->workingFrame", workingFrame);
}

void cvFrameDiff::convertColor(Show t){
	cvtColor(workingFrame, workingFrame, CV_BGR2GRAY);
	if (t == SHOW) imshow("convertColor->workingFrame", workingFrame);
}

void cvFrameDiff::threshold(int val1, int val2, Show t){
	// TODO: still need to fix/upgrade this function
	// to use the two value in an addaptive way (range)
	// not one value
	cv::threshold(workingFrame, workingFrame, val1, 255, THRESH_BINARY); //val1 was 12
		
	
	//cv::medianBlur(workingFrame, workingFrame, 17);
	
	//cv::erode(workingFrame, workingFrame, Mat(12, 12, CV_8UC1), Point(-1, -1), 8);  //3,2 or 3,3

	//cv::dilate(workingFrame, workingFrame, Mat(8, 8, CV_8UC1), Point(-1, -1), 4);

	//cv::erode(workingFrame, workingFrame, Mat(8, 8, CV_8UC1), Point(-1, -1), 5);  //3,2 or 3,3

	////cv::dilate(workingFrame, workingFrame, Mat(8, 8, CV_8UC1), Point(-1, -1), 3);

	//cv::erode(workingFrame, workingFrame, Mat(3, 3, CV_8UC1), Point(-1, -1), 8);  //3,2 or 3,3

	if (t == SHOW) imshow("threshold->workingFrame", workingFrame);
}

void cvFrameDiff::median(int matWidth, Show t){
	medianBlur(workingFrame, workingFrame, matWidth); //matWidth was 11
	if (t == SHOW) imshow("median->workingFrame", workingFrame);
}

void cvFrameDiff::erode(int matWidth, Show t){
	cv::erode(workingFrame, workingFrame, Mat(matWidth, matWidth, CV_8UC1), Point(-1, -1), 2);  //3,2 or 3,3
	if (t == SHOW) imshow("erode->workingFrame", workingFrame);
}

Point cvFrameDiff::findCenter(vector <Point> cont){
	int sumX = 0, sumY = 0;
	for (int i = 0; i < cont.size(); i++){
		sumX += cont[i].x;
		sumY += cont[i].y;
	}
	return Point(sumX / cont.size(), sumY / cont.size());
}

void cvFrameDiff::findContours(Show t){
	// find contours:
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	cv::findContours(workingFrame, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);

	workingFrame = cv::Mat::zeros(workingFrame.rows, workingFrame.cols, CV_8UC3);
	cv::RNG rng(12345);
	for (int i = 0; i < contours.size(); i++)
	{
		if (cv::contourArea(contours[i]) > 0) {
			cv::Scalar color = cv::Scalar(rng.uniform(50, 255), rng.uniform(50, 255), rng.uniform(50, 255));
			drawContours(workingFrame, contours, i, color, 1, 8, hierarchy, 0);

			//fill the data structure
			contour dumm;
			dumm.pixelCount = contours[i].size();
			dumm.points = contours[i];
			if (dumm.pixelCount < 2) continue;
			dumm.center = findCenter(contours[i]);
			dumm.boundingRect = boundingRect(contours[i]);
			int leastDistancetoLastFrameHands = findSmallestDistance(dumm.center);
			if (leastDistancetoLastFrameHands < 50) {
				this->Contours.push_back(dumm);
				rectangle(workingFrame, dumm.boundingRect, Scalar(0, 0, 255));
			}
			//circle(imgWithContours, dumm.center, 4.0, Scalar(0, 0, 255), 1, 8);

		}
	}
	if (t == SHOW) imshow("findContours->workingFrame", workingFrame);
}

int cvFrameDiff::findSmallestDistance(Point p){
	if (prevFrameHands.hand1.center.x == 0 && prevFrameHands.hand1.center.y == 0) return 0;
	Point diff = p - prevFrameHands.hand1.center;
	int distance = sqrt(diff.ddot(diff));
	int smallest = distance;
	diff = p - prevFrameHands.hand2.center;
	distance = sqrt(diff.ddot(diff));
	if (distance < smallest) smallest = distance;
	return smallest;
}

void cvFrameDiff::suggestHands(Show t){

	////////////////////////////////////////////
	////===================================
	int k = howmanycenters;
	suggestedHandPoints.clear();  //vector <KM> kmeans;  //not actually kmeans by definition 
	//if Initial position of prevFrames
	if (prevFrameHands.hand1.center.x == 0 && prevFrameHands.hand1.center.y == 0 &&
		prevFrameHands.hand2.center.x == 0 && prevFrameHands.hand2.center.y == 0)
		for (int n = 0; n < k; n++)
			suggestedHandPoints.push_back(Point(320 - (320 / (k - 1))*n, 120));
	else{ //if not initial position of prevFrames, then use them for the first two kmeans centroids
		//TODO: a7ot el points el kant m3aia el frame el fat , we lw l k akbr mn 2 , yb2a
		//akamel l points el fadla be eni ashof el bwazan gy fi ani no2ta we fi ani etgah watwaka3 mkan l noise wa7oto 
		// // dlwa2ty ha3ml random // btw dlwa2ty msh hzawed el k asln
		suggestedHandPoints.push_back(prevFrameHands.hand1.center);
		suggestedHandPoints.push_back(prevFrameHands.hand2.center);
		for (int n = 2; n < k; n++){
			suggestedHandPoints.push_back(Point(rand() % 320, rand() % 240));
		}
	}////////===============================
	//////////////////////////////////////////////////////////

	// keda m3ana el initial Ks

	//now to get nearest point of every centroid
	//kda gbna el initial positions -> n3ml b2a k means
	///////////////=======================================
	for (int bb = 0; bb < Contours.size(); bb++){
		int nearestKmeanIndex = 0;
		int nearestKmeanDist = 10000000;
		for (int cc = 0; cc < suggestedHandPoints.size(); cc++){
			Point diff = suggestedHandPoints[cc].center - Contours[bb].center;
			int distance = sqrt(diff.ddot(diff));
			if (distance <= nearestKmeanDist){
				nearestKmeanDist = distance;
				nearestKmeanIndex = cc;
			}
		}
		suggestedHandPoints[nearestKmeanIndex].contoursNearMe.push_back(Contours[bb]);
	}///////=======================================
	/////////////////////////////////////////////////////////

	//ana m7sabtesh el centers hna asln lel suggestedHandPoints lesa
	//fa i have nothing to show here , lesa ba3d el kam function call l gayin 

	//now calculate the average of their positions using the enhancing functions
	/////// calculate new k center regarding the weight of each contour
	calculateContoursCenter_weighted();
	if (t == SHOW){
		for (int i = 0; i < suggestedHandPoints.size(); i++){
			circle(workingFrame, suggestedHandPoints[i].center, 4.0, Scalar(255, 255, 255), 1, 8);
		}
		imshow("suggestedHands_wghted->" + to_string(howmanycenters), workingFrame);
	}

	/////// shift the centers according to their position in the window
	shiftHandCenter_AccordingToClusterPoisition();
	if (t == SHOW){
		for (int i = 0; i < suggestedHandPoints.size(); i++){
			circle(workingFrame, suggestedHandPoints[i].center, 4.0, Scalar(255, 0, 0), 1, 8);
		}
		imshow("suggestedHands_shifted->" + to_string(howmanycenters), workingFrame);
	}


}

void cvFrameDiff::calculateContoursCenter_weighted(){
	for (int i = 0; i < suggestedHandPoints.size(); i++){  // le kol k 
		int sumx = 0, sumy = 0; //hagib sum x we sum y
		int sumPixels = 0; //dlwa2ty hagama3 el pixel count bta3 kol contour
		for (int j = 0; j < suggestedHandPoints[i].contoursNearMe.size(); j++){
			sumPixels += suggestedHandPoints[i].contoursNearMe[j].pixelCount;
		} //we m3aia sum pixels
		for (int j = 0; j < suggestedHandPoints[i].contoursNearMe.size(); j++){
			sumx += suggestedHandPoints[i].contoursNearMe[j].center.x * suggestedHandPoints[i].contoursNearMe[j].pixelCount / sumPixels;
			sumy += suggestedHandPoints[i].contoursNearMe[j].center.y * suggestedHandPoints[i].contoursNearMe[j].pixelCount / sumPixels;
		}
		suggestedHandPoints[i].center.x = sumx;
		suggestedHandPoints[i].center.y = sumy;


		//if (suggestedHandPoints[i].contoursNearMe.size() != 0){
		//	suggestedHandPoints[i].center.x = sumx / suggestedHandPoints[i].contoursNearMe.size();
		//	suggestedHandPoints[i].center.y = sumy / suggestedHandPoints[i].contoursNearMe.size();
		//}
	}

}

//naive approach to the problem
void cvFrameDiff::shiftHandCenter_AccordingToClusterPoisition(){
	for (int i = 0; i < suggestedHandPoints.size(); i++){
		int distancefromcenter = suggestedHandPoints[i].center.y - (240 / 2); //rkam
		float ratio = (float)distancefromcenter / (240 / 2); //ratio from -1 : 0 : 1 

		int maxheight = -10000, minnheight = 10000;
		//fin max hight and min hight in contours of this suggestedHandPoints.contoursNearme as it's [bounding box]
		for (int j = 0; j < suggestedHandPoints[i].contoursNearMe.size(); j++){
			int highestPointhere = suggestedHandPoints[i].contoursNearMe[j].center.y + suggestedHandPoints[i].contoursNearMe[j].boundingRect.height / 2;
			int lowestPointhere = suggestedHandPoints[i].contoursNearMe[j].center.y - suggestedHandPoints[i].contoursNearMe[j].boundingRect.height / 2;
			if (highestPointhere > maxheight) maxheight = highestPointhere;
			if (lowestPointhere < minnheight) minnheight = lowestPointhere;
		}
		int clusterheight = maxheight - minnheight;
		int delta = ratio * clusterheight / 2; // /2 3ashan a2alel el delta bs
		suggestedHandPoints[i].center.y = suggestedHandPoints[i].center.y + delta;
	}
}

void cvFrameDiff::voteFor2GoodHands(Show t){
	//if not initial frame
	if (!(prevFrameHands.hand1.center.x == 0 && prevFrameHands.hand1.center.y == 0 &&
		prevFrameHands.hand2.center.x == 0 && prevFrameHands.hand2.center.y == 0)){
		//mafrod hna b2a maths ktir , kalman we prediction lel k we tagarob we mean value lel distance
		// we atwka3 mkan l noise 3ashan a7otlha el k we keda lghayet mla2i mkan l 2id !!!!!!!!!

		no2GoodHands = false;
		int distanceThreshold = 50; // TODO say enha 20, bs b3d keda 3aizin n3ml tari2a azka
		// IMPORTANT!!!
		// el rakam da 5atir awi , lw olayel awei msh bal7a2 7raket el 2id el sri3a
		// we lw kber mni , byb2a moshkla fi en el 2 hands byshofo nafsohom orybin le nafs el point
		// we be keda by7awlo y2rabo we yb2o nafs el no2ta kol frame , w yrga3o tb3n le aamakenhom we 
		// y discard-o l mkan da fa byefdalo sabteen 5ales :'(

		//FIRST HAND
		//now i need to find smallest distance between a hand and every suggested hand
		int nearestsuggestedHandIndex = 0;
		int nearestsuggestedHandDist = 10000000;
		for (int cc = 0; cc < suggestedHandPoints.size(); cc++){
			Point diff = suggestedHandPoints[cc].center - prevFrameHands.hand1.center;
			//Point diff = suggestedHandPoints[cc].center - lazyNazumiHands_prev.hand1.center;
			int distance = sqrt(diff.ddot(diff));
			if (distance <= nearestsuggestedHandDist){
				nearestsuggestedHandDist = distance;
				nearestsuggestedHandIndex = cc;
			}
		} //if least distance is within range then it is a good hand position
		if (nearestsuggestedHandDist < distanceThreshold){
			currentFrameHands.hand1 = suggestedHandPoints[nearestsuggestedHandIndex];
		}
		else {
			no2GoodHands = true; // :( 
		}

		//same code for 
		//SECOND HAND
		//now i need to find smallest distance between a hand and every suggested hand
		nearestsuggestedHandIndex = 0;
		nearestsuggestedHandDist = 10000000;
		for (int cc = 0; cc < suggestedHandPoints.size(); cc++){
			Point diff = suggestedHandPoints[cc].center - prevFrameHands.hand2.center;
			//Point diff = suggestedHandPoints[cc].center - lazyNazumiHands_prev.hand2.center;
			int distance = sqrt(diff.ddot(diff));
			if (distance <= nearestsuggestedHandDist){
				nearestsuggestedHandDist = distance;
				nearestsuggestedHandIndex = cc;
			}
		} //if least distance is within range then it is a good hand position
		if (nearestsuggestedHandDist < distanceThreshold){
			currentFrameHands.hand2 = suggestedHandPoints[nearestsuggestedHandIndex];
		}
		else {
			no2GoodHands = true; // :( 
		}



	}
	else { //initial case , 5od el 2 hands zy mahoma 
		currentFrameHands.hand1 = suggestedHandPoints[0];
		currentFrameHands.hand2 = suggestedHandPoints[1];
		no2GoodHands = false;
	}

	if (t == SHOW){
		
		circle(workingFrame, currentFrameHands.hand1.center, 4.0, Scalar(0, 255, 0), 1, 8);
		circle(workingFrame, currentFrameHands.hand2.center, 4.0, Scalar(0, 255, 0), 1, 8);
		imshow("currentFrameHands -> " + to_string(howmanycenters), workingFrame);
	}
}

void cvFrameDiff::solveHandsNoise_usingIncrementedK(Show t){

	if (currentFrameHands.hand1.center.x == currentFrameHands.hand2.center.x && currentFrameHands.hand1.center.y == currentFrameHands.hand2.center.y){
		//lw ba2o nafs el no2ta
		currentFrameHands.hand1 = prevFrameHands.hand1;
		currentFrameHands.hand2 = prevFrameHands.hand2;
		//raga3hom lel frame el fat we hope en el gy yb2a a7san
	}


	////di mkanha msh hena , mlhash function asln 
	if (t == SHOW){
		Mat shower;
		currentFrame.copyTo(shower);
		//cout << "x: " << currentFrameHands.hand1.center.x << "y: " << currentFrameHands.hand1.center.y << endl;
		//cout << "x: " << currentFrameHands.hand2.center.x << "y: " << currentFrameHands.hand2.center.y << endl;
		circle(shower, currentFrameHands.hand1.center, 4.0, Scalar(0, 255, 0), 3, 8);
		circle(shower, currentFrameHands.hand2.center, 4.0, Scalar(0, 255, 0), 3, 8);
		imshow("test1", shower);
	}

	if (!no2GoodHands) return;

	if (howmanycenters > 4){
		cout << "can't solve noise conflicting with hand position, please go somewhere still." << endl;
		no2GoodHands = false;

		// 5ali balak!! -> kda lw m3aia wa7da mn el etnen hahmelha wa2ol fi error
		//currentFrameHands.hand1 = prevFrameHands.hand1;
		//currentFrameHands.hand2 = prevFrameHands.hand2;
	}

	//REMEMBER BASSEM!
	howmanycenters++;
	suggestHands(NOSHOW);
	voteFor2GoodHands(NOSHOW);


}

void cvFrameDiff::reEnhanceHands(){


	if (prevFrameHands.hand1.center.x == 0 && prevFrameHands.hand1.center.y == 0 && prevFrameHands.hand2.center.x == 0 && prevFrameHands.hand2.center.y == 0)
		return;
	////test -- naiveXSmoothing 
	// output = output + ALPHA * (input - output);
	//float ALPHA = 0.3f;  //lma drabtha fi 3 fl x , kda b2ol lel data (shokrannn rawa7 lomak :V ) kda ba5od 0.9 mn l adim 
	//currentFrameHands.hand1.center.x = currentFrameHands.hand1.center.x + ALPHA * 2.2f * (prevFrameHands.hand1.center.x - currentFrameHands.hand1.center.x);
	//currentFrameHands.hand2.center.x = currentFrameHands.hand2.center.x + ALPHA * 2.2f * (prevFrameHands.hand2.center.x - currentFrameHands.hand2.center.x);

	/// HERE WAS BEST POSITIONS OF HANDS
	
	//ev->setRightStickPos(glm::vec2(currentFrameHands.hand2.center.x, currentFrameHands.hand2.center.y));
	//ev->setLeftStickPos(glm::vec2(currentFrameHands.hand1.center.x, currentFrameHands.hand1.center.y));

}

void cvFrameDiff::lazyNazumi(){
	//current frame hand da yo3tbar el mouse dlwa2ty 
	// fa da mafrod y7arak el previous frame hand
	// we 3aiz atba3 el prv frame hand di fl curve we b3d kda ht3amel m3aha

	float limitDistance = 8;
	
	//hand1
	float TotalDistance = sqrt(pow(lazyNazumiHands_current.hand1.center.y - currentFrameHands.hand1.center.y, 2) + pow(lazyNazumiHands_current.hand1.center.x - currentFrameHands.hand1.center.x, 2));
	if (TotalDistance > limitDistance){

		//lazyNazumiHands_prev = lazyNazumiHands_current;

		float xDelta, yDelta;
		xDelta = (currentFrameHands.hand1.center.x - lazyNazumiHands_current.hand1.center.x) * (TotalDistance - limitDistance) / TotalDistance;
		yDelta = (currentFrameHands.hand1.center.y - lazyNazumiHands_current.hand1.center.y) * (TotalDistance - limitDistance) / TotalDistance;
		lazyNazumiHands_current.hand1.center.x = lazyNazumiHands_current.hand1.center.x + xDelta;
		lazyNazumiHands_current.hand1.center.y = lazyNazumiHands_current.hand1.center.y + yDelta;
	}
	//hand2
	TotalDistance = sqrt(pow(lazyNazumiHands_current.hand2.center.y - currentFrameHands.hand2.center.y, 2) + pow(lazyNazumiHands_current.hand2.center.x - currentFrameHands.hand2.center.x, 2));
	if (TotalDistance > limitDistance){

		//lazyNazumiHands_prev = lazyNazumiHands_current;

		float xDelta, yDelta;
		xDelta = (currentFrameHands.hand2.center.x - lazyNazumiHands_current.hand2.center.x) * (TotalDistance - limitDistance) / TotalDistance;
		yDelta = (currentFrameHands.hand2.center.y - lazyNazumiHands_current.hand2.center.y) * (TotalDistance - limitDistance) / TotalDistance;
		lazyNazumiHands_current.hand2.center.x = lazyNazumiHands_current.hand2.center.x + xDelta;
		lazyNazumiHands_current.hand2.center.y = lazyNazumiHands_current.hand2.center.y + yDelta;
	}
	ev->setLeftStickPos(glm::vec2(lazyNazumiHands_current.hand1.center.x, lazyNazumiHands_current.hand1.center.y));
	ev->setRightStickPos(glm::vec2(lazyNazumiHands_current.hand2.center.x, lazyNazumiHands_current.hand2.center.y));

}


void cvFrameDiff::NaiveHitCheck(){

	/*float Vx1 = (currentFrameHands.hand1.center.x - prevFrameHands.hand1.center.x);
	float Vy1 = (currentFrameHands.hand1.center.y - prevFrameHands.hand1.center.y);
	float Vx2 = (currentFrameHands.hand2.center.x - prevFrameHands.hand2.center.x);
	float Vy2 = (currentFrameHands.hand2.center.y - prevFrameHands.hand2.center.y);*/
	float Vx1 = (lazyNazumiHands_current.hand1.center.x - lazyNazumiHands_prev.hand1.center.x);
	float Vy1 = (lazyNazumiHands_current.hand1.center.y - lazyNazumiHands_prev.hand1.center.y);
	float Vx2 = (lazyNazumiHands_current.hand2.center.x - lazyNazumiHands_prev.hand2.center.x);
	float Vy2 = (lazyNazumiHands_current.hand2.center.y - lazyNazumiHands_prev.hand2.center.y);

	//this must be here, but it also happen every frame after the lazyNazumi fucntion
	lazyNazumiHands_prev = lazyNazumiHands_current;

	hit_data temp1, temp2;

	temp1.p = lazyNazumiHands_current.hand1.center;
	temp1.Vx = Vx1;
	temp1.Vy = Vy1;
	temp1.accel = 0.0;

	temp2.p = lazyNazumiHands_current.hand2.center;
	temp2.Vx = Vx2;
	temp2.Vy = Vy2;
	temp2.accel = 0.0;

	hits[0].push_back(temp1);
	hits[1].push_back(temp2);

	float V1, V2, acc;
	for (int i = 0; i < 2; i++)
		if (hits[i].size() >= 2){

			V1 = sqrt((hits[i][hits[i].size() - 1].Vx)*(hits[i][hits[i].size() - 1].Vx) + (hits[i][hits[i].size() - 1].Vy)*(hits[i][hits[i].size() - 1].Vy));
			V2 = sqrt((hits[i][hits[i].size() - 2].Vx)*(hits[i][hits[i].size() - 2].Vx) + (hits[i][hits[i].size() - 2].Vy)*(hits[i][hits[i].size() - 2].Vy));

			acc = (V1 - V2);
			hits[i][hits[i].size() - 1].accel = acc;
		}

	if (hits[0].size() > 6 || hits[1].size() > 6){

		int x = 0;
		if (hits[0][hits[0].size() - 3].Vy > 0.0 &&hits[0][hits[0].size() - 2].Vy > 0.0 &&hits[0][hits[0].size() - 1].Vy <= 0.0 && hits[1][hits[1].size() - 3].Vy > 0.0 &&hits[1][hits[1].size() - 2].Vy > 0.0 &&hits[1][hits[1].size() - 1].Vy <= 0.0)
			x++;

		if ( hits[0][hits[0].size() - 3].Vy>0.0 &&hits[0][hits[0].size() - 2].Vy>0.0 &&hits[0][hits[0].size() - 1].Vy<=0.0){
			this->sendHitToGUI(hits[0][hits[0].size() - 1].accel, glm::vec2(hits[0][hits[0].size() - 1].p.x, hits[0][hits[0].size() - 1].p.y));
			cout << "LEFT hit " << hitCount << ": " << hits[0][hits[0].size() - 1].accel << endl;
			hitCount++;
		}

		if (hits[1][hits[1].size() - 3].Vy>0.0 &&hits[1][hits[1].size() - 2].Vy>0.0 &&hits[1][hits[1].size() - 1].Vy <= 0.0){
			this->sendHitToGUI(hits[1][hits[1].size() - 1].accel, glm::vec2(hits[1][hits[1].size() - 1].p.x, hits[1][hits[1].size() - 1].p.y));
			cout << "RIGHT hit " << hitCount << ": " << hits[1][hits[1].size() - 1].accel << endl;
			hitCount++;			
		}
	}
}
void cvFrameDiff::smoothWindow(){
	//circle1 = lerp(circle1, lastFrameData[0], 0.7f);
	//circle2 = lerp(circle2, lastFrameData[1], 0.7f);
	Point circle1 = currentFrameHands.hand1.center;
	Point circle2 = currentFrameHands.hand2.center;
	cv::Mat imgCirclesSmooth = cv::Mat::zeros(240 * 2, 320 * 2, CV_8UC1);
	circle(imgCirclesSmooth, Point(circle1.x * 2, circle1.y * 2), 3.0, 255, 2, 8);
	circle(imgCirclesSmooth, Point(circle2.x * 2, circle2.y * 2), 3.0, 255, 2, 8);
	imshow("smoothWindow", imgCirclesSmooth);


}

cvFrameDiff::~cvFrameDiff()
{
}
