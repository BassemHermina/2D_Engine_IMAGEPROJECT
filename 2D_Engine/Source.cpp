
/////////// todo
// * resize window - Done
// * a3ml ai sora soghyara mdawara keda wraha shafaf (a3ml enable transparency)
// * ha3ml smoothing we stablization le arkam l position bta3 el LR wel RR el gayin mn l camera processing 
// * after finishing the GUI prototype, I need to to re optimize it and reduce shader/drawing calls and buffer bindings IMPPPP!!
// * needs fixing for the behaviour of adding same motion before it ends
float frame = 0, Time, Timebase = 0, fps;

#include "includes.h" //these are the common OpenGL and libraries includes, not my code
#include "ShaderClass.h" //this one includes "includes.h" so it can't be included inside it, leave it here
#include "Sprite.h" //this one includes "includes.h" so it can't be included inside it, leave it here
#include "GameObject.h"
#include "Scene.h"
#include "scripts.h"
#include "EventSystem.h"
#include "drumController.h"
#include "buttonController.h"
#include "Drum.h"
#include "cvFrameDiff.h"
#include "cvHough.h"


double MouseX, MouseY;
GLFWwindow* window;
void initializeOpenGL(int, int);
void FPS(std::string op);

int main(void)
{
	initializeOpenGL(1200, 900);
	EventSystem * events = EventSystem::getEventSystem(window);
	
	cvHough cvlogic(events);
	cvFrameDiff CV(events);
	
	cvlogic.releaseCam();
	CV.initializeCam();
	// the structure will not be totally dependant on the update functions
	// there will be a class to handle user input, and events that talks to the desired object's controller
	// and then the desired controller will decide what to do with it's modelClass
	// -> think of the use cases and diagrams for this

	// - fix the transform updating problem and fix the transforms themselves (el mwdo3 kolo)
	// - build rough outs for the GUI scenes (menu, settings, game [2 levels maybe..]) in images and icons
	// - build the Scenes in the engine
	// - create the drum class -> contains reference to drumController
	// - create the drum controller class (that talks with the event system, then decides and tells the drum class)
	// - agawed ? shaders le3b kda :D w mmkn special effects lw 3ereft
	
	//OpenGL higher that 3.X needs an active VertexArrayObject to run properly, wana lesa msh fahem hwa lazmto a , aw taghyiro 
	GLuint VertexArrayID;	glGenVertexArrays(1, &VertexArrayID);	glBindVertexArray(VertexArrayID);
	
	////////////////////////////
	// Create ,compile and get our uniforms of our first shader
	ShaderClass normalShader;
	normalShader.LoadShaders("Shaders/SpriteShader_normal.vs", "Shaders/SpriteShader_normal.fs");
	normalShader.getUniformLocation(2, "Transform" ,"myTextureSampler"); //get handles of unifroms inside the shader
	////////////////////////////

	//mdam homa by3mlo assign le nafshom fl event handler awel ma yt3mlo
	//yb2a homa mtrtbin fi el vector nafs el trtib da
	// yb2a a5ali el EventSystem yemshi 3lehom mn wara le odam
	// we lma yla2i hit (hyla2i ezay?) mykmelsh l loop
	Scene drumsScene;

	Drum drum1(glm::vec2(507, (900 - 650)), "Assets/DDS/1.dds", &normalShader, window, NULL, NULL, &drumsScene); //start / update
	drum1.transform.setRefScale(glm::vec2(1.2, 1.2)); drum1.setSoundFileName("Assets/sound/snare.wav");
	Drum drum2(glm::vec2(780, (900 - 700)), "Assets/DDS/2.dds", &normalShader, window, NULL, NULL, &drumsScene); //start / update
	drum2.transform.setRefScale(glm::vec2(1.2, 1.2)); drum2.setSoundFileName("Assets/sound/low tom.wav");
	Drum drum3(glm::vec2(720, (900 - 470)), "Assets/DDS/2.dds", &normalShader, window, NULL, NULL, &drumsScene); //start / update
	drum3.transform.setRefScale(glm::vec2(1.2, 1.2)); drum3.setSoundFileName("Assets/sound/mid tom.wav");
	Drum drum4(glm::vec2(500, (900 - 320)), "Assets/DDS/2.dds", &normalShader, window, NULL, NULL, &drumsScene); //start / update
	drum4.transform.setRefScale(glm::vec2(1.2, 1.2)); drum4.setSoundFileName("Assets/sound/hitom.wav");
	Drum drum5(glm::vec2(200, (900 - 500)), "Assets/DDS/7.dds", &normalShader, window, NULL, NULL, &drumsScene); //start / update
	drum5.transform.setRefScale(glm::vec2(1.2, 1.2)); drum5.setSoundFileName("Assets/sound/hihat.wav");
	Drum drum6(glm::vec2(300, (900 - 200)), "Assets/DDS/6.dds", &normalShader, window, NULL, NULL, &drumsScene); //start / update
	drum6.transform.setRefScale(glm::vec2(1.2, 1.2)); drum6.setSoundFileName("Assets/sound/crash.wav");
	Drum drum7(glm::vec2(930, (900 - 220)), "Assets/DDS/8.dds", &normalShader, window, NULL, NULL, &drumsScene); //start / update
	drum7.transform.setRefScale(glm::vec2(1.2, 1.2)); drum7.setSoundFileName("Assets/sound/ride.wav");
	Drum dog(glm::vec2(1000, (900 - 650)), "Assets/DDS/14.dds", &normalShader, window, NULL, NULL, &drumsScene); //start / update
	dog.transform.setRefScale(glm::vec2(0.4, 0.4)); dog.setSoundFileName("Assets/sound/dog.wav");
	Drum handle1(glm::vec2(200, 200), "Assets/DDS/10.dds", &normalShader, window, NULL, handleFollowLeftStick, &drumsScene); //start / update
	handle1.transform.setRefScale(glm::vec2(1.0, 1.0));
	Drum handle2(glm::vec2(200, 200), "Assets/DDS/11.dds", &normalShader, window, NULL, handleFollowRightStick, &drumsScene); //start / update
	handle2.transform.setRefScale(glm::vec2(1.0, 1.0));
	Drum exitButton(glm::vec2(1150, 40), "Assets/DDS/15.dds", &normalShader, window, NULL, NULL, &drumsScene); //start / update
	exitButton.transform.setRefScale(glm::vec2(0.8, 0.8));
	exitButton.drumc->setOnHit(exitMode);
	events->setCurrentScene(&drumsScene);

	//the Update Script is for the automatic behaviour of the gameobjects (which don't require and event to be triggered)
	//but is called/done every frame update
	int wait = 0;

	Scene selectionScene;
	Drum sticksImage(glm::vec2(300, (900 - 400)), "Assets/DDS/13.dds", &normalShader, window, NULL, NULL, &selectionScene); //start / update
	sticksImage.transform.setRefScale(glm::vec2(1, 1)); sticksImage.setSoundFileName("Assets/sound/snare.wav");
	sticksImage.drumc->setOnHit(selectSticksMode);
	Drum HandsImage(glm::vec2(900, (900 - 400)), "Assets/DDS/12.dds", &normalShader, window, NULL, NULL, &selectionScene); //start / update
	HandsImage.transform.setRefScale(glm::vec2(1, 1)); HandsImage.setSoundFileName("Assets/sound/snare.wav");
	HandsImage.drumc->setOnHit(selectHandsMode);
	Drum selectionHadle(glm::vec2(200, 200), "Assets/DDS/10.dds", &normalShader, window, NULL, handleFollowLeftStick, &selectionScene); //start / update
	selectionHadle.transform.setRefScale(glm::vec2(1.0, 1.0));
	events->setCurrentScene(&selectionScene);

	unsigned long int tt = 0;
	do{
		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		// Move forward
		if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS){  
			drum1.motionlib.assignmotion(CURVE_EaseinOut_jump, MOT_translate, AXIS_Y, 400, 0.0, 50);
		}
		if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS){
			drum1.motionlib.assignmotion(CURVE_EaseinOut_jump, MOT_translate, AXIS_X, 400, 0.0, 50);
			//sprite1.motionlib.assignmotion(CURVE_dashElastic, MOT_scale, AXIS_X_Y, 300, 0.0, 0.15);
		}

		if (!mode1 && !mode2 && !once){
			CV.initializeCam();
			cvlogic.releaseCam();
			once = true;
			events->setCurrentScene(&selectionScene);
		}

		if (mode1 && once){
			once = false;
			events->setCurrentScene(&drumsScene);
		}
		else if (mode2 && once){
			once = false; 
			events->setCurrentScene(&drumsScene);
			CV.releaseCam();
			cvlogic.initializeCam();
		}

		if (mode2)
			cvlogic.doWork();
		/// ----
		else {
			if (!CV.getFrame(NOSHOW)) break;  //end of video
			CV.frameDifference(NOSHOW);
			CV.convertColor(NOSHOW);
			CV.threshold(12, 12, NOSHOW);
			CV.median(11, NOSHOW);
			CV.erode(3, NOSHOW);
			CV.findContours(NOSHOW);
			CV.suggestHands(NOSHOW);
			CV.voteFor2GoodHands(NOSHOW);
			CV.solveHandsNoise_usingIncrementedK(SHOW); //SHOW!
			CV.reEnhanceHands();
			//m3aia el points bta3et el edeen
			CV.lazyNazumi();
			//CV.smoothWindow();
			CV.NaiveHitCheck();
		}
		/*
		//disco mode :D hahaha
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS){
			wait++;
			if (wait > 25){
				glClearColor(((float)(rand() % 100) / 100.0f), ((float)(rand() % 100) / 100.0f), ((float)(rand() % 100) / 100.0f), 0.0f);
				wait = 0;
				drum1.motionlib.assignmotion(CURVE_dashElastic, MOT_translate, AXIS_X, 400, 0.0, 50);
				drum2.motionlib.assignmotion(CURVE_EaseinOut_jump, MOT_translate, AXIS_Y, 400, 0.0, 60);
				drum3.motionlib.assignmotion(CURVE_dashElastic, MOT_translate, AXIS_X_Y, 420, 0.0, 35);
				drum4.motionlib.assignmotion(CURVE_dashElastic, MOT_translate, AXIS_X, 380, 0.0, 39);
				drum5.motionlib.assignmotion(CURVE_EaseOutElastic, MOT_translate, AXIS_Y, 390, 0.0, 150);
				drum6.motionlib.assignmotion(CURVE_dashElastic, MOT_translate, AXIS_X_Y, 390, 0.0, 45);
				drum7.motionlib.assignmotion(CURVE_dashElastic, MOT_translate, AXIS_Y, 400, 0.0, 39);
				drum8.motionlib.assignmotion(CURVE_EaseinOut_jump, MOT_translate, AXIS_X, 411, 0.0, 47);
				drum9.motionlib.assignmotion(CURVE_dashElastic, MOT_translate, AXIS_X_Y, 420, 0.0, 50);

			}
				//drum1.motionlib.assignmotion(CURVE_EaseinOut_jump, MOT_translate, AXIS_X, 400, 0.0, 50);
		}
		*/
		

		FPS("console");

		//if (windowwww){
		//	glfwSetWindowSize(window, 640, 480);
		//	windowwww = false;
		//}


		// Scene. updatedata() -> render() -> dumpGameobjectsDeltas()
		if (mode1 || mode2){
			drumsScene.updatedata();
			//game logic here
			events->checkForEvents();
			//sprite3.transform.Translate(sprite4.transform.Position); //keda ana fhmt - keda hayfollow ba3d bezabt
			drumsScene.render();
			drumsScene.dumpGameobjectsDeltas();
		}
		else { //selection mode
			selectionScene.updatedata();
			events->checkForEvents();
			selectionScene.render();
			selectionScene.dumpGameobjectsDeltas();
		}
		
		glDisable(GL_BLEND);
		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();
		

	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
	glfwWindowShouldClose(window) == 0);

	
	glDeleteVertexArrays(1, &VertexArrayID); //this one stays here

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}


void initializeOpenGL(int w , int h){

	// Initialise GLFW
	if (!glfwInit()) {
		fprintf(stderr, "Failed to initialize GLFW\n");
		exit(0);
		return;
	}

	
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(w, h, "window", NULL, NULL);
	if (window == NULL){
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		glfwTerminate();
		exit(0);
		return;
	}
	glfwMakeContextCurrent(window);

	// Hide the mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	// Hide the mouse and enable unlimited mouvement
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		exit(0);
		return;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Dark blue background
	//glClearColor(23.0f / 255.0f, 103.0f / 255.0f, 164.0f / 255.0f, 0.0f);
	glClearColor(249.0f / 255.0f, 53.0f / 255.0f, 53.0f / 255.0f, 0.0f);
	// Enable depth test
	//glEnable(GL_DEPTH_TEST); //da by5alih yrsm mn odam le wara msh 3aref leeh, we bybawaz el blending , 5las balash mno 5ales! :D 
	// Accept fragment if it closer to the camera than the former one
	//glDepthFunc(GL_LESS);
}

void FPS(std::string op){
	unsigned long int currentTime = glutGet(GLUT_ELAPSED_TIME);
	//grabt fta7t AMD settings -> Gaming -> Global settings -> wait for vertical refresh (true) 5alla el frame rate 60 bzabt
	frame++;
	Time = glutGet(GLUT_ELAPSED_TIME); //da lw hasta3mlo 7a2i2i fi arkam lazm unsigned long int
	if (Time - Timebase > 1000) {
		fps = frame * 1000.0 / (Time - Timebase);
		Timebase = Time;
		frame = 0;
		if (op == "console"){
			printf("fps: %f\n", fps);
		}
		else if (op == "window"){

		}
	}

	
}
