#include "Transform.h"


Transform::Transform(glm::vec2 initialPos, GLFWwindow * wind){
	this->RefPos = initialPos;
	this->Position = initialPos; //da el bnt3amel m3ah -- l real da 3ashan l render ya5do bas 
	this->Rotation = glm::vec3(0.0f, 0.0f, 0.0f);
	this->Scale = glm::vec2(1.0f, 1.0f);
	this->window = wind;
	//posReal is position regarding the shader (0,0) is in the center of screen
	//Position is the position used as a logic (0,0) is at bottom left corner
	//delta is half the window width and half the window height
	
	int windowW, windowH;
	glfwGetWindowSize(this->window, &windowW, &windowH);
	this->posDelta = glm::vec2(windowW / 2, windowH / 2);

	this->posReal.x = this->Position.x - this->posDelta.x;
	this->posReal.y = this->Position.y - this->posDelta.y;
}

void Transform::setRotation(glm::vec3 r){
	this->Rotation = r;
}

void Transform::setPosition(glm::vec2 p){
	this->Position = p;
	this->posReal.x = this->Position.x - this->posDelta.x;
	this->posReal.y = this->Position.y - this->posDelta.y;
}


void Transform::setScale(glm::vec2 s){
	this->Scale = s;
}

void Transform::setRefPosition(glm::vec2 p){
	this->RefPos = p;
	updateData();
}

void Transform::setRefScale(glm::vec2 s){
	this->RefScale = s;
	updateData();
}

//takes the whole distance that will be translated, as it will be cleared at the frame end
//unlike setRefPos, which is kept for the rest of the game
void Transform::Translate(glm::vec2 d){
	this->frameDeltaPos += d;
	updateData();
}

void Transform::Rotate(glm::vec3 r){
	//TODO
	//this->Rotation = this->Rotation
	//updateData()
}

void Transform::SScale(glm::vec2 s){
	this->frameDeltaSca += s;
	updateData();
}

void Transform::updateData(){
	this->setPosition(this->RefPos + this->frameDeltaPos);
	this->setScale(this->RefScale + this->frameDeltaSca);

}

void Transform::dumpDeltas(){
	this->frameDeltaPos = glm::vec2(0,0);
	this->frameDeltaSca = glm::vec2(0, 0);
}

Transform::~Transform()
{
}
