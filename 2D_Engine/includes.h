#pragma once
// Include standard headers
#include <stdio.h>
#include <stdlib.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
//#include <glfw3.h>
#include <GLFW/glfw3.h>

#include <GL/freeglut.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>
//using namespace glm;

//we abl kol da e3ml NuGet glew we nupengl core
//common da folder mn bara, we ana 7atet el folder el parent fl includes mn Project->Properties->C++/General->additional Dependencies -> add the parentfoldername/.
//then add the required cpp files to the project source files, we add el header files lel project Header files 
#include "textureLoad.hpp"

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <map>

#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/video/background_segm.hpp>
#include <time.h>
#include "opencv2\features2d.hpp"



#include <SFML\Audio.hpp>

enum CURVE{
	CURVE_first,
	CURVE_EaseOutCubic,
	CURVE_EaseOutElastic,
	CURVE_EaseinOut_jump,
	CURVE_dashElastic,
	CURVE_last
};

enum MOTION{
	MOT_first,
	MOT_translate,
	MOT_rotate,
	MOT_scale,
	MOT_last
};

enum AXIS{
	AXIS_first,
	AXIS_X,
	AXIS_Y,
	AXIS_X_Y,
	AXIS_last
};


enum Show{
	SHOW,
	NOSHOW
};

struct contour{
	std::vector <cv::Point> points;
	cv::Point center;
	int pixelCount;
	cv::Rect boundingRect;
};

struct Hand{
	Hand(cv::Point p){
		this->center = p;
	};
	Hand(){}; //mfihosh 7aga , mfrod a3ml fih 7aga??
	cv::Point center;
	std::vector <contour> contoursNearMe;

	cv::Scalar color;
	cv::Point oldcenter; //used in the kmeans function to keep track of the old center
};

struct Hands{
	Hand hand1;
	Hand hand2;
};
