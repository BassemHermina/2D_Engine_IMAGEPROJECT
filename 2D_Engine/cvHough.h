#pragma once
#include "includes.h"
#include "EventSystem.h"
#include <vector>
#include <iostream>
#include <string>
#include <ctype.h>



struct hit_data
{
	//Point p1;
	cv::Point p;
	long double Vx, Vy;
	long double accel;

};


class cvHough
{

	int Y_MIN = 0;
	int Y_MAX = 240;
	int Cr_MIN = 130;
	int Cr_MAX = 255;
	int Cb_MIN = 46;
	int Cb_MAX = 255;
	int frameCnt = 0;
	long cnt_hit1 = 0;
	long cnt_hit2 = 0;

	int thresh1 = 1, thresh2 = 8, distThresh = 5; //thresh1 for canny lower threshold , higher is 2*lower, thresh2 foraccumlated circles

	std::vector<cv::Vec3f> circles;
	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;

	cv::Mat gray, prevGray, image, imageOut, frame, diffGray, grayState, prevGrayState, edges, imageGray, tstMat, toBeRemoved, skinned, imageWithoutSkin, skinned2, imageWithoutSkin2, GrayforHough;


	cv::Ptr<cv::BackgroundSubtractorMOG2> bgm;
	int64 prev_clock = 0, curr_clock = 0;
	std::vector<std::vector <cv::Point>>sticks; //2
	std::vector<std::vector <hit_data>>hits; //2
	cv::Mat finalOut;

	cv::VideoCapture *cap;

	cv::Mat getSkin(cv::Mat);

	int bassemFrameCounter = 0;
	long double second = 0;

	float getDistance(cv::Point, cv::Point);
	void calc_velocity();
	void detectHit();
	void reduce_vector(std::vector<cv::Point> &v);
	void reduce_vector(std::vector<hit_data> &v);


	EventSystem * ev;
	void sendHitToGUI(float, glm::vec2);

	Hands lazyNazumiHands_current;
	Hands lazyNazumiHands_prev;


public:
	cvHough(EventSystem *);
	
	void initializeCam();
	void releaseCam();
	void lazyNazumi();
	void Initialize();
	void help();
	void doWork();


	~cvHough();
};


