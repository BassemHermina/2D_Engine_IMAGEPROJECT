#include "GameObject.h"
#pragma once

GameObject::GameObject(glm::vec2 TransPos, const char * file, GLFWwindow * wind, GameObject * gameobject) : transform{ TransPos, wind },
texture{ file, wind }, motionlib{ gameobject }
{
	
}


GameObject::~GameObject()
{
}
