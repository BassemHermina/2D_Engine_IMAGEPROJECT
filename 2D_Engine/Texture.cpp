#include "Texture.h"


Texture::Texture(const char * file, GLFWwindow * wind){
	this->window = wind;
	this->fileLocation = file;
	std::string n = file;
	bool dds = false;
	if (n.find(".dds") != std::string::npos || n.find(".DDS") != std::string::npos)
		dds = true;
	
	if (dds){
		this->TextureID = loadDDS(file);
		this->getImageResDDSTYPE(file);
	}
	else {
		this->TextureID = loadBMP_custom(file);
		this->getImageRes(file);
	}
	
	
	
	
	this->makeBuffers();
}

void Texture::getImageResDDSTYPE(const char * imagepath){
	
	unsigned char header[124];
	FILE *fp;

	/* try to open the file */
	fp = fopen(imagepath, "rb");
	if (fp == NULL){
		printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath); getchar();
		return;
	}

	/* verify the type of file */
	char filecode[4];
	fread(filecode, 1, 4, fp);
	if (strncmp(filecode, "DDS ", 4) != 0) {
		fclose(fp);
		return;
	}

	/* get the surface desc */
	fread(&header, 124, 1, fp);

	unsigned int height = *(unsigned int*)&(header[8]);
	unsigned int width = *(unsigned int*)&(header[12]);

	this->imageHeight = (float)height;
	this->imageWidth = (float)width;

	/* close the file pointer */
	fclose(fp);
		
	return ;
}

void Texture::getImageRes(const char * imagepath){
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;

	// Open the file
	FILE * file = fopen(imagepath, "rb");
	if (!file){
		printf("%s could not be opened. Are you in the right directory?\n", imagepath);
		getchar();
		return;
	}
	if (fread(header, 1, 54, file) != 54){
		printf("Not a correct BMP file\n");
		fclose(file);
		return;
	}

	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);

	this->imageHeight =(float)height;
	this->imageWidth = (float)width;

	// Everything is in memory now, the file can be closed.
	fclose(file);
}

void Texture::makeBuffers(){
	//these should be centered in the half of the screen to make the scaling , rotating, ... work properly
	//create a quad with the image ratio size to the screen
	//starting form the bottom left corner
	//t7t shemal
	int iScreenWidth, iScreenHeight;
	glfwGetWindowSize(this->window, &iScreenWidth, &iScreenHeight);
	float ScreenWidth = iScreenWidth;
	float ScreenHeight = iScreenHeight;

	g_vertex_buffer_data[0] = -1 * (imageWidth / ScreenWidth);
	g_vertex_buffer_data[1] = -1 * (imageHeight / ScreenHeight);
	g_vertex_buffer_data[2] = 0.0f;
	//fo2 shemal
	g_vertex_buffer_data[3] = -1 * (imageWidth / ScreenWidth);
	g_vertex_buffer_data[4] = 1 * (imageHeight / ScreenHeight);
	g_vertex_buffer_data[5] = 0.0f;
	//t7t ymin
	g_vertex_buffer_data[6] = 1 * (imageWidth / ScreenWidth);
	g_vertex_buffer_data[7] = -1 * (imageHeight / ScreenHeight);
	g_vertex_buffer_data[8] = 0.0f;
	//fo2 shemal
	g_vertex_buffer_data[9] = -1 * (imageWidth / ScreenWidth);
	g_vertex_buffer_data[10] = 1 * (imageHeight / ScreenHeight);
	g_vertex_buffer_data[11] = 0.0f;
	//fo2 ymin
	g_vertex_buffer_data[12] = (imageWidth / ScreenWidth);
	g_vertex_buffer_data[13] = (imageHeight / ScreenHeight);
	g_vertex_buffer_data[14] = 0.0f;
	//t7t ymin
	g_vertex_buffer_data[15] = 1 * (imageWidth / ScreenWidth);
	g_vertex_buffer_data[16] = -1 * (imageHeight / ScreenHeight);
	g_vertex_buffer_data[17] = 0.0f;


	g_uv_buffer_data[0] = 0.0f;//t7t shemal
	g_uv_buffer_data[1] = 0.0f;
	g_uv_buffer_data[2] = 0.0f;//fo2 shemal
	g_uv_buffer_data[3] = 1.0f;
	g_uv_buffer_data[4] = 1.0f;//t7t ymin
	g_uv_buffer_data[5] = 0.0f;
	g_uv_buffer_data[6] = 0.0f;//fo2 shemal
	g_uv_buffer_data[7] = 1.0f;
	g_uv_buffer_data[8] = 1.0f;//fo2 ymin
	g_uv_buffer_data[9] = 1.0f;
	g_uv_buffer_data[10] = 1.0f;//t7t ymin
	g_uv_buffer_data[11] = 0.0f;

	glGenBuffers(1, &vertexbufferID);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

	glGenBuffers(1, &uvbufferID);
	glBindBuffer(GL_ARRAY_BUFFER, uvbufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_uv_buffer_data), g_uv_buffer_data, GL_STATIC_DRAW);

}

Texture::~Texture()
{
}
