#include "drumController.h"
#include "Drum.h"
#include "EventSystem.h"

drumController::drumController()
{
}

void drumController::setOnHit(void (* func)()){
	this->onHit = func;
}

void drumController::leftMouseClick(glm::vec2 pos){
	//position starts (0,0) at top left corner :/
	//printf("I got the click: %f, %f\n", pos.x, pos.y);
	
	//check if inside bounds
	if (checkinsideBounds(pos)){
		//use the hit function now
		simulateHit(90);
	}
		
}

void drumController::rightMouseClick(glm::vec2 pos){
	if (checkinsideBounds(pos)){
		//testing animations
		// AHAHA verry funny :D
		//drum->motionlib.assignmotion(CURVE_EaseOutElastic, MOT_scale, AXIS_X, 600, 0, 5.0);
		//drum->motionlib.assignmotion(CURVE_EaseOutElastic, MOT_scale, AXIS_Y, 800, 0, 3.0);
		//drum->motionlib.assignmotion(CURVE_dashElastic, MOT_scale, AXIS_X_Y, 1500, 0, 0.4);

		// alaaah laziza awi lama bt accumulate (di a7la)
		drum->motionlib.assignmotion(CURVE_EaseOutElastic, MOT_scale, AXIS_X, 600, 0, 3.0);
		drum->motionlib.assignmotion(CURVE_EaseOutElastic, MOT_scale, AXIS_Y, 900, 0, 2.0);
		drum->playSound(100, 0);
	}
}

void drumController::hit(float vel, glm::vec2 pos){
		
	//check if inside bounds
	if (checkinsideBounds(pos)){
		//use the hit function now
		simulateHit(vel);
	}
}

void drumController::simulateHit(float power){
	drum->motionlib.assignmotion(CURVE_dashElastic, MOT_scale, AXIS_X_Y, 200, 0, 1.0);
	drum->playSound(power, 0);
	if (onHit)
		this->onHit();
	EventSystem * ev = EventSystem::getEventSystem();
	ev->gotArightHit();
}

void drumController::mouseMoved(glm::vec2 pos){
	//check if inside bounds
	bool checkInside = checkinsideBounds(pos);
	if (checkInside && !mouseInside){ // enter
		//printf("mouse enter!\n");
		mouseInside = true;
		//drum->motionlib.assignmotion(CURVE_EaseOutCubic, MOT_scale, AXIS_X_Y, 115, 0, 0.06); //this way failed :( 5azalteni ya motionLib
	}
	else if (!checkInside && mouseInside){
		//printf("mouse leave.. \n");
		mouseInside = false;
		//drum->motionlib.assignmotion(CURVE_EaseOutCubic, MOT_scale, AXIS_X_Y, 115, 0, -0.06);
	}

	// woow eshtghalet sa7 !
	if (mouseInside){
		drum->transform.SScale(glm::vec2(scalingdelta, scalingdelta));
		if (scalingdelta < 0.03)
			scalingdelta += 0.005;
	}
	else {
		drum->transform.SScale(glm::vec2(scalingdelta, scalingdelta));
		if (scalingdelta > 0)
			scalingdelta -= 0.005;
	}
}


Scene * drumController::getScene(){
	return this->drum->scene;
}

bool drumController::checkinsideBounds(glm::vec2 pos){
	RECT t;
	t.left = drum->transform.Position.x - (drum->texture.imageWidth / 2) * drum->transform.Scale.x;
	t.right = drum->transform.Position.x + (drum->texture.imageWidth / 2) * drum->transform.Scale.x;
	t.top = drum->transform.Position.y + (drum->texture.imageHeight / 2) * drum->transform.Scale.y;
	t.bottom = drum->transform.Position.y - (drum->texture.imageHeight / 2) * drum->transform.Scale.y;

	if (pos.x >= t.left && pos.x <= t.right && pos.y >= t.bottom && pos.y <= t.top)
		return true;
	else return false;

}

void drumController::setDrum(Drum * d){
	this->drum = d;
}

drumController::~drumController()
{
}
