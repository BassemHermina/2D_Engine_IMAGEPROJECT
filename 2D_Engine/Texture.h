#pragma once
#include "includes.h"
#include "Transform.h"

class Texture
{
private:
	std::string fileLocation; //done
	GLfloat g_vertex_buffer_data[18];
	GLfloat g_uv_buffer_data[12];
	void getImageRes(const char *);
	void getImageResDDSTYPE(const char *);
	void makeBuffers();
	GLFWwindow * window;
public:
	GLuint TextureID; //done
	GLuint vertexbufferID;
	GLuint uvbufferID;

	Texture(const char * , GLFWwindow *);
	float imageWidth;
	float imageHeight;
	~Texture();
};

