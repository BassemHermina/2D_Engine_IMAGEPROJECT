#version 330 core

// Interpolated values from the vertex shaders
in vec2 UV;

// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform sampler2D myTextureSampler;

void main(){

	// Output color = color of the texture at the specified UV
    //if (texture( myTextureSampler, UV ).a == 0.0) discard;
	color = texture( myTextureSampler, UV );
}