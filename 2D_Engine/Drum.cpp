#include "Drum.h"
#include <math.h>
#include "Scene.h"
#include "EventSystem.h"

//constructor:
//takes initial x, y position of the sprite, loads the image using loadBPM_custom and save
//it then uses the screen resolution and the image resolution
//to calculate vertex buffer data of the quad it would be rendered on, the uv buffer data is 
//constant for all sprites (as this is not a spite sheet!) 
Drum::Drum(glm::vec2 TransPos, const char * file, ShaderClass * shader, GLFWwindow * wind, void(*start)(GameObject * me), void(*update)(GameObject * me), Scene * scene)
	: GameObject{ TransPos, file, wind, this }
{
	this->setOnStart(start);
	this->setOnUpdate(update);
	this->window = wind;
	this->myshader = shader;
	this->scene = scene;
	this->scene->addGameobject(this);
	glfwGetWindowSize(window, &this->ScreenWidth, &this->ScreenHeight);

	//I don't save any reference to the controller except in the eventSystem, as nothing else shall communicate with the controller
	//edit: i will save a reference in the drum itself , as to easily debug the project
	drumController * drumc = new drumController; //should be initialized like that
	this->drumc = drumc;
	EventSystem * events = EventSystem::getEventSystem();
	drumc->setDrum(this);

	events->addMouseListener(0, (Controller*)(drumc));
	events->addMouseListener(1, (Controller*)(drumc));
	events->addMouseMovementListener((Controller*)(drumc));
	events->addHitListener((Controller*)(drumc));

	//call function Start 
	if (Start != NULL)
		(*Start)(this);
}

//this is pre defined for the current shader, and not accurate to be used with different shaders
//and this is not accurate, the sprites are supposed to be of different types and use many different shaders
//still have to fix this TODO
void Drum::render(){
	//use myShader
	glUseProgram(myshader->ID);



	//generate translation matrix 4x4 (we only need 2, 1 3ashan l shader myz3alsh and 1 homogenous )
	glm::mat4 translationMat4x4 = glm::mat4(1.0f);
	translationMat4x4 = glm::translate(glm::mat4(), glm::vec3(this->transform.posReal.x * 2 / ScreenWidth, this->transform.posReal.y * 2 / ScreenHeight, 0.0)); //this vector in screenspace 0:2

	//generate scale matrix 4x4 (we only need 2, 1 3ashan l shader myz3alsh and 1 homogenous )
	glm::mat4 scalingMat4x4 = glm::mat4(1.0f);
	scalingMat4x4 = glm::scale(glm::mat4(), glm::vec3(this->transform.Scale, 1.0)); //this vector in screenspace 0:2

	glm::mat4 Transform = translationMat4x4 * scalingMat4x4;

	// Send our transformation to the currently bound shader
	glUniformMatrix4fv(this->myshader->UniformID("Transform"), 1, GL_FALSE, &Transform[0][0]);

	// Bind our texture in Texture Unit 0
	//msh 3aref mmkn nstfad ezay mn ba2i el texture slots
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->texture.TextureID);
	// send the texture to our fragment shader in its location uniform
	glUniform1i(this->myshader->UniformID("myTextureSampler"), 0); //this 0 is the texture slot number : GL_TEXTURE0

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0); //vertexPostion location = 0 in vertex shader
	glBindBuffer(GL_ARRAY_BUFFER, this->texture.vertexbufferID);
	glVertexAttribPointer(
		0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, this->texture.uvbufferID);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);

	// Draw the triangle !
	glDrawArrays(GL_TRIANGLES, 0, 2 * 3); // 12*3 indices starting at 0 -> 12 triangles

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);


}

void Drum::setOnStart(void(*start)(GameObject *)){
	this->Start = start;
}

void Drum::setOnUpdate(void(*update)(GameObject *)){
	this->Update = update;
}

void Drum::setSoundFileName(std::string s){
	this->soundFileName = s;
	if (!this->soundBuffer.loadFromFile(this->soundFileName))
	{
		printf("error creating sound!");
	}

	this->soundObject.setBuffer(this->soundBuffer);
	
}

void Drum::playSound(float vol, float pitch ){
	//msh 3arfin lesa a l default value bta3et el pitch
	//this->soundObject.setPitch(pitch);
	//quick hack protection
	if (this->soundFileName == "NONE") return;
	this->soundObject.setVolume(vol);
	this->soundObject.play();
}

Drum::~Drum()
{
	glDeleteBuffers(1, &this->texture.vertexbufferID);
	glDeleteBuffers(1, &this->texture.uvbufferID);
	glDeleteTextures(1, &this->texture.TextureID);
}

