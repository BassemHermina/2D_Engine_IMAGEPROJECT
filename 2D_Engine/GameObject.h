#pragma once
#include "Transform.h"
#include "Texture.h"
#include "ShaderClass.h"
#include "motionLib.h"

extern class Scene;

class GameObject
{
private:

public:
	GameObject(glm::vec2 TransPos, const char * file, GLFWwindow * wind, GameObject *);
	Transform transform;
	Texture texture;
	motionLib motionlib;
	ShaderClass * myshader;
	void(*Update)(GameObject * me);
	void(*Start)(GameObject * me);
	Scene * scene;


	int ScreenWidth;
	int ScreenHeight;

	virtual void render() = 0; //polymorphism
	// mfish update, mna sheltaha
	~GameObject();
};

