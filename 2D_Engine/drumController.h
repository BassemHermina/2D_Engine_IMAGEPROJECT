#pragma once
#include "Controller.h"

extern class Drum;

class drumController : Controller
{
private:
	Drum * drum;
	float scalingdelta = 0; //used inside the hover function as a hack
	void(*onHit)();
public:
	drumController();
	
	// virtual functions, must be implemented when inherit from Controller
	void leftMouseClick(glm::vec2 pos);
	void rightMouseClick(glm::vec2 pos);
	void hit(float vel, glm::vec2 pos);
	void mouseMoved(glm::vec2 pos);
	Scene * getScene();
	
	void simulateHit(float );
	void setOnHit(void(*)());
	bool checkinsideBounds(glm::vec2 pos);
	void setDrum(Drum *);
	bool mouseInside = false;
	~drumController();
};

