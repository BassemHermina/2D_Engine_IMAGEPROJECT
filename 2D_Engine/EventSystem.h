#pragma once
#include "drumController.h"
#include "Controller.h"
#include "includes.h"
#include "Scene.h"

class EventSystem
{
	/*
	*	EventSystem Class:
	*	- Singleton class
	*	- use the static function getEventSystem() to return a pointer for the class
	*/
	

private:
	// the instance pointer to the current EventSystem 
	static EventSystem * instance;
	
	Scene * currentScene;
	GLFWwindow* window;

	// the Listeners list Upcasted -> to be dynamic casted to be used
	std::vector <Controller*> hitListeners;
	std::vector <Controller*> leftClickMouseListeners;
	std::vector <Controller*> rightClickMouseListeners;
	std::vector <Controller*> hoverListeners;

	double mouseX, mouseY;
	int windowWidth, windowHeight;

	// check for mouse events with if elses.
	// called every frame 
	void checkForMouseClicks();
	void checkForMouseMovement();
	void updateMouseParametersAndWindowSize();


	bool leftmousedown = false;
	bool rightmousedown = false;

	glm::vec2 leftStickLoc;
	glm::vec2 rightStickLoc;

	unsigned long int lastHitTime = 0;

public:
	// used to get the eventSystem singleton
	static EventSystem * getEventSystem(GLFWwindow* window = nullptr);



	// add a controller to the MouseListener vector
	void addMouseListener(int button, Controller * c);

	// add a controller to the hitListener vector
	void addHitListener(Controller *c);

	// add a controller to the  vector
	void addMouseMovementListener(Controller *c);



	// simulate a mouse click on the screen
	void simulateMouseClick(int button, glm::vec2 pos);

	//todo: simulateHit() then implement drumcontroller and buttonController 
	// simulate a hit on the screen
	void simulateHit(float vel, glm::vec2 pos);
	// float velocity, float acceleration, glm::vec2 direction
	// ask Mina 3ashan ydini l m3lomat 3n l 7agat di ??

	glm::vec2 getMouseLocation();

	void setCurrentScene(Scene *);
	void checkForEvents();

	void gotArightHit();
	glm::vec2 getLeftStickLocation();
	glm::vec2 getRightStickLocation();
	void setLeftStickPos(glm::vec2 pos);
	void setRightStickPos(glm::vec2 pos);

	EventSystem(); //don't use this for initialization an eventSystem
	~EventSystem();
};

