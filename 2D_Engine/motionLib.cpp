#include "motionLib.h"
#include "Sprite.h"

double easeOutCubic(double t) { return (--t)*t*t + 1; }
double easeOutElastic(double t) { if (t == 1) return 0; return .04 * t / (--t) * sin(25 * t); }
double easeinOut_jump(double t) { return (1 - ((2 * t - 1)*(2 * t - 1))); }
double dashElastic(double t) { if (t == 0) t = 0.001;  return (((-log10f(13 * t) + 1) * 1) * sinf(18.85 * t) / 3); }

void motionLib::translate(int index, float value, AXIS axis){
	//ana mthya2li en l motion dayman hyb2a relative lel position bta3 el object
	if (axis == AXIS_X)
		this->mygameobject->transform.Translate(glm::vec2(value, 0));
	if (axis == AXIS_Y)
		this->mygameobject->transform.Translate(glm::vec2(0, value));
	if (axis == AXIS_X_Y)
		this->mygameobject->transform.Translate(glm::vec2(value, value));
}
void motionLib::scale(int index, float value, AXIS axis){
	if (axis == AXIS_X_Y)
		this->mygameobject->transform.SScale(glm::vec2(value, value));
	else if (axis == AXIS_X)
		this->mygameobject->transform.SScale(glm::vec2(value, 0));
	else if (axis == AXIS_Y)
		this->mygameobject->transform.SScale(glm::vec2(0, value));
}


motionLib::motionLib(GameObject * gameobject)
{	
	mygameobject = gameobject;
	///////////// IMPORTANT!!!
	myFunctionsMap[CURVE_EaseOutCubic] = easeOutCubic; //a7ot asma2 el functions el basta5dmha fl animating hena
	myFunctionsMap[CURVE_EaseOutElastic] = easeOutElastic;
	myFunctionsMap[CURVE_EaseinOut_jump] = easeinOut_jump;
	myFunctionsMap[CURVE_dashElastic] = dashElastic;
	myAxisFunctionsMap[MOT_translate] = &motionLib::translate; //el tri2a di tl3etli fl intellisense ta7t, ana kont kateb zy l fo2 dol 3adi
	myAxisFunctionsMap[MOT_scale] = &motionLib::scale;

	if (myFunctionsMap.size() < (int)CURVE_last - 1)
		assert(0); //add the missing curves functions to myFunctionMap
	//still have not added rotation function to axisFunctions
}

void motionLib::check(AXIS ax, MOTION pro, CURVE mot){
	//should re do this part as an ENUM and check if strings are part of ENUM or not
	if (pro < MOT_first || pro > MOT_last)
		assert(0);
	if (ax < AXIS_first || ax > AXIS_last) //da hyboz fl rotation
		assert(0);
	if (mot < CURVE_first || pro > CURVE_last)
		assert(0);
}
void motionLib::assignmotion(CURVE name, MOTION affectedproperty, AXIS affectedAxis, int duration, float curveStart, float curveEnd){
	bool q = false;
	check(affectedAxis, affectedproperty, name);
	//just for now - el traslation we7esh awi la y accumulate, bs el scale latif we shaklo laziz we hwa by accumulate
	if (this->contains(name, affectedAxis, affectedproperty) && affectedproperty == MOT_translate) //3ashan may7otesh kaza motion fi nafs el wa2t , just for fun y3ni
		//q = true;		//it was return;						// di 3ashan ana mkontesh m-handle en el keyboard byedini 100 click lama adoso
		return;														// fa lma b3ml 100 click keda el animation bybooz 5ales
	
	//better solution . if found , then put it in a waiting queue

	motionData * anim = new motionData;
	anim->animationName = name;
	anim->affectedProperty = affectedproperty;
	anim->affectedAxis = affectedAxis;
	anim->duration = duration;
	anim->starting_time = glutGet(GLUT_ELAPSED_TIME);
	anim->curveStart_value = curveStart;
	anim->curveEnd_value = curveEnd;

	//this will always save one waiting animation (the last one sent) obbss :/ lw 5ragt we d5lt w 5ragt ..
	// better to save only once
	if (q) {
		if (waiting == NULL)
			waiting = anim; 
	}
	else
		motions.push_back(*anim);
}

bool motionLib::contains(CURVE mot, AXIS ax, MOTION pro){
	for (int i = 0; i < motions.size(); i++){
		if (motions[i].animationName == mot && motions[i].affectedAxis == ax && motions[i].affectedProperty == pro)
			return true;
	}
	return false;
}

void motionLib::updatemotions(){
	bool lastTime = false;
	for (int i = 0; i < motions.size(); i++){
		unsigned long int t = (glutGet(GLUT_ELAPSED_TIME) - motions[i].starting_time);
		if (t > motions[i].duration) {
			//remove the animation as it's done (sometimes , the time is passed when the sent value to function is 0.9991, so the final 
			//value of the function is not retunred yet, so i need to explicitly do it here 
			lastTime = true;

			//solve for not returning to same point - removed the continue
			//continue;
			t = motions[i].duration;

		}
		//mfrod hna andah el easingFuncion/animation
		double bebi = t / motions[i].duration;		// [0 -> 1]
		double tt = myFunctionsMap[motions[i].animationName](bebi);
		
		//if (bebi > 0.3)
		//	printf("debug");

		//we hya traga3li rakam 
		//fa a5od el rakam wandh function l property 3ashan a3mlha update bl rakam l gdid
		double factor = motions[i].curveStart_value + tt * (motions[i].curveEnd_value - motions[i].curveStart_value);
		
		//el member funtion pointer lazm object mn l class hwa el yndahha
		(this->*myAxisFunctionsMap[motions[i].affectedProperty])(i, factor, motions[i].affectedAxis);
		

		if (lastTime){
			//if the motion was to return it to its position/scale, then no harm.
			//but if the equation was made to end not at 0 delta, there fore save the last frame as the ref for next :O woow! so easy
			//bwazet el denia :(
			//this->mygameobject->transform.setRefScale(this->mygameobject->transform.RefScale + this->mygameobject->transform.frameDeltaSca);
			//this->mygameobject->transform.setRefPosition(this->mygameobject->transform.RefPos + this->mygameobject->transform.frameDeltaPos);
			motions.erase(motions.begin() + i);
			lastTime = false;
			i--;

			//if the motionLibrary don't contain the animation that caused this animation to be waiting (if it was the just-removed motion)
			//then i will push it in the motions vector! the waiting is over
			if (waiting != NULL)
			if (!this->contains(waiting->animationName, waiting->affectedAxis, waiting->affectedProperty)){
				assignmotion(waiting->animationName, waiting->affectedProperty, waiting->affectedAxis, waiting->duration, waiting->curveStart_value, waiting->curveEnd_value);
				waiting = NULL;
				i++;
			}

			
		}


	}
}

void motionLib::stopmotion(int index){
	if (this->motions.size() >= index){
		index--;
		motions.erase(motions.begin() + index);
	}
}


motionLib::~motionLib()
{
}
