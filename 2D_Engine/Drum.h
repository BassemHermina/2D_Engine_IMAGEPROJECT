#pragma once
#include "includes.h"
#include "ShaderClass.h"
#include "GameObject.h"
#include "drumController.h"

//#include "Scene.h"  //hna lw di hna brdo tb2a circular include , fa 7atetha extern class we 7atetha fl cpp


extern class Scene;

class Drum : public GameObject  //inherts public GameObject : public to make these accessible in the controller class
{
private:
	//INHERITS:
	// - transform
	// - texture
	// - motionLib
	// - *myshader
	// - virtual void render()
	// - void(*Update)(GameObject * me); 
	// - void(*Start)(GameObject * me);
	// - *Scene
	// - windowheight, width

	GLFWwindow * window;

	sf::Sound soundObject;
	sf::SoundBuffer soundBuffer;
	std::string soundFileName = "NONE";

public:

	//constructor (refPosition, imageFile, shader, window, StartFunction, UpdateFunction, Scene)
	Drum(glm::vec2 TransPos, const char * file, ShaderClass * shader, GLFWwindow * wind, void(*start)(GameObject * me), void(*upate)(GameObject* me), Scene * scene);

	void setOnUpdate(void(*)(GameObject *)); //takes a pointer to function of type of Update function 
	void setOnStart(void(*)(GameObject *)); //takes a pointer to function of type of Start function

	//here create another customUpdate function - which takes the argument with the same kind of the class
	//which i will set and call manually 
	// void(*Update)(Sprite * me);

	drumController * drumc; // NEW!!!!!!!!!!!!	 use the drum to access the drum controller(for now) / which will call the drum :V 
	
	void Drum::setSoundFileName(std::string s);
	void Drum::playSound(float vol, float pitch);

	//INHERITS virtual function render
	//render the sprite using its current shader
	void render();

	~Drum();
};

