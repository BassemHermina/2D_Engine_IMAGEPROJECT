#include "EventSystem.h"
#include "includes.h"

EventSystem * EventSystem::instance = nullptr; //initializing the static pointer to null (required by system)
// (EventSystem *) : it's type
// (EventSystem::) : class as it is static
// (instance)	   : it's name

EventSystem * EventSystem::getEventSystem(GLFWwindow* window){
	if (!instance){
		instance = new EventSystem;
		instance->window = window;
	}
	//printf("returning event system ..\n");
	return instance;
}


//called by whoever needs the current mousex and y 
glm::vec2 EventSystem::getMouseLocation(){
	glfwGetCursorPos(this->window, &this->mouseX, &this->mouseY);
	this->mouseY = this->windowHeight - this->mouseY; //first frame window size would be wrong, as the checkForEvents() function was not called before
	return glm::vec2(this->mouseX, this->mouseY);
}

glm::vec2 EventSystem::getLeftStickLocation(){
	return leftStickLoc;
}

glm::vec2 EventSystem::getRightStickLocation(){
	return rightStickLoc;
}

void EventSystem::setLeftStickPos(glm::vec2 pos){
	//quick hack to fix that the CV origin is at top left, and my origin is bottom left
	pos.y = pos.y * (float)((float)windowHeight / 240); //240 is the window height of the CV window
	pos.x = pos.x * (float)((float)windowWidth / 320); //320 is the window height of the CV window
	pos.y = windowHeight - pos.y;

	this->leftStickLoc = pos;
}

void EventSystem::setRightStickPos(glm::vec2 pos){
	//quick hack to fix that the CV origin is at top left, and my origin is bottom left
	pos.y = pos.y * (float)((float)windowHeight / 240); //240 is the window height of the CV window
	pos.x = pos.x * (float)((float)windowWidth / 320); //320 is the window height of the CV window
	pos.y = windowHeight - pos.y;


	this->rightStickLoc = pos;
}

void EventSystem::checkForEvents(){
	updateMouseParametersAndWindowSize();

	checkForMouseClicks();
	checkForMouseMovement();
}

void EventSystem::updateMouseParametersAndWindowSize(){
	glfwGetCursorPos(this->window, &this->mouseX, &this->mouseY);
	glfwGetWindowSize(this->window, &this->windowWidth, &this->windowHeight);
	//flip y of mouse (make origin (0,0) at bottom left)
	this->mouseY = this->windowHeight - this->mouseY;
}

void EventSystem::checkForMouseMovement(){
	//hack for hit noise, see simulateHit()
	lastHitTime++;
	
	// overhead ! every frame check for cursor position and window size !
		for (int i = 0; i < hoverListeners.size(); i++){
			if (hoverListeners[i]->getScene() == currentScene)
				hoverListeners[i]->mouseMoved(glm::vec2(this->mouseX, this->mouseY));
		}
}

void EventSystem::setCurrentScene(Scene * s){
	this->currentScene = s;
}

void EventSystem::checkForMouseClicks(){
	
	//no dragging: i didn't implement it
	if (glfwGetMouseButton(this->window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
		leftmousedown = true;

	else if (glfwGetMouseButton(this->window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
		rightmousedown = true;

	//this was for the keyboard buttons
	//if (glfwGetKey(this->window, GLFW_BUTTON) == GLFW_PRESS){
	
	if (glfwGetMouseButton(this->window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE && leftmousedown){
		leftmousedown = false;
		// printf("mouse clicked!\n");
		// di lma bt3raf en fi click, bt3ml simulate lel click
		simulateMouseClick(0, glm::vec2((float)this->mouseX, (float)this->mouseY));
	}

	else if (glfwGetMouseButton(this->window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_RELEASE && rightmousedown){
		rightmousedown = false;
		// printf("mouse clicked!\n");
		// di lma bt3raf en fi click, bt3ml simulate lel click
		simulateMouseClick(1, glm::vec2((float)this->mouseX, (float)this->mouseY));
	}
}

void EventSystem::addMouseListener(int button, Controller * c){
	if (button == 0){
		leftClickMouseListeners.push_back(c);
	}
	else if (button == 1){
		rightClickMouseListeners.push_back(c);
	}
	else printf("Error! Mouse button %d not defined!", button);
	
}

void EventSystem::addMouseMovementListener(Controller *c){
	hoverListeners.push_back(c);
}


void EventSystem::addHitListener(Controller *c){
	hitListeners.push_back(c);
}

void EventSystem::simulateMouseClick(int button, glm::vec2 pos){
	if (button == 0){
		for (int i = 0; i < leftClickMouseListeners.size(); i++){
			//tb a lazmet el dynamic_cast keda mna keda keda handah el function l virtual
			//drumController * drumcontroller = dynamic_cast<drumController*> (leftClickMouseListeners[i]);
			if (hoverListeners[i]->getScene() == currentScene)
				leftClickMouseListeners[i]->leftMouseClick(pos);
		}
	} else if (button == 1){
		for (int i = 0; i < rightClickMouseListeners.size(); i++){
			//tb a lazmet el dynamic_cast keda mna keda keda handah el function l virtual
			//drumController * drumcontroller = dynamic_cast<drumController*> (leftClickMouseListeners[i]);
			if (hoverListeners[i]->getScene() == currentScene)
				rightClickMouseListeners[i]->rightMouseClick(pos);
		}
	}
}

void EventSystem::simulateHit(float vel, glm::vec2 pos){

	//if (lastHitTime < 8){
	//	std::cout << "NO! -> LAST HIT WAS SOON ENOUGH : " << lastHitTime << std::endl;
	//	return;
	//}
	
	//lastHitTime = 0;  //wrong placement for the hack -> here we dan't assure that this was a hit on a drum, so any drum should 
	// tell the event system tht it got hit
 
	//quick hack to fix that the CV origin is at top left, and my origin is bottom left
	pos.y = pos.y * (float)((float)windowHeight / 240); //240 is the window height of the CV window
	pos.x = pos.x * (float)((float)windowWidth / 320); //320 is the window height of the CV window
	pos.y = windowHeight - pos.y;
	vel = vel * 100;
	if (vel < 0) vel = -vel;
	if (vel > 100) vel = 100; //protect speakers
	//std::cout << "velocity of hit: " << vel << ", x: " << pos.x << ", y: " << pos.y << std::endl;

	for (int i = 0; i < hitListeners.size(); i++){
		if (hoverListeners[i]->getScene() == currentScene)
			hitListeners[i]->hit(100, pos);
	}
}

void EventSystem::gotArightHit(){
	lastHitTime = 0;
}



EventSystem::EventSystem()
{
	printf("event system initialized!\n");
}

EventSystem::~EventSystem()
{
	printf("event system destroyed!\n");
}
