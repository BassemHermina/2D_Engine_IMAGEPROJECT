#pragma once
#include "includes.h"

struct uniformNamesID{
	GLuint ID;
	GLchar * nameInShader;
};

class ShaderClass
{
private:

public:
	ShaderClass();
	
	//load the shader file and compile it (function copied from tutorial 5)
	GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);
	
	//shader programID
	GLuint ID; 
	
	//vector of uniforms contained in this shader
	std::vector<uniformNamesID> uniformsOfShader;

	//initialize the vector of uniforms (number of uniforms, uniformName1, uniformName2, ....)
	void getUniformLocation( int count, ...);

	//get a uniform ID by its name
	GLuint UniformID(const char *);

	~ShaderClass();
};

