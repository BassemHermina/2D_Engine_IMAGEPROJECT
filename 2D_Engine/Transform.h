#pragma once
#include "includes.h"

class Transform
{
private:
	glm::vec2 posDelta;
	glm::vec2 scaleDelta;
	GLFWwindow* window;

	//translation
	void setPosition(glm::vec2); //every frame the logic sets the numbers for the renderer 
	void setRotation(glm::vec3); //every frame the logic sets the numbers for the renderer 
	void setScale(glm::vec2);	 //every frame the logic sets the numbers for the renderer 

public:
	glm::vec2 frameDeltaPos;
	glm::vec2 frameDeltaSca;

	glm::vec2 RefPos; // we set this value as users
	glm::vec2 Position; // equals RefPos + delta -> // calculated every frame (to get it, not set it as users)
	glm::vec2 posReal; //for the shader -- calculated from Position

	glm::vec2 RefScale; // we set this value as users
	glm::vec2 Scale; // equals RefPos + delta -> // calculated every frame (to get it, not set it as users)

	glm::vec3 Rotation;
	
	RECT rect; //when shall it be updated ?
			   //i think in a built in update function called
			   //in the gameLoop before any code is called

	Transform(glm::vec2 initialPos, GLFWwindow *);
	void setRefPosition(glm::vec2);
	void setRefScale(glm::vec2);

	// Translate -> adds delta to frameDelta which will be accumulated at end of frame to posReal and Position
	void Translate(glm::vec2);
	// Rotate -> adds delta to rotation
	void Rotate(glm::vec3);
	// SScale -> adds delta to scale
	void SScale(glm::vec2);

	//i think these should be here , lesa msh 3aref mfrod yb2o fen
	//bs homa kol l hy3mloh hay2saro fi matrix/vector bytb3et lel shader
	// we da hy2sar fl render , zay l matric bta3 el Transform l bytb3et
	// lel shader bl scale wel translate da
	//void skew(t[0->1] , b[start of curve] , d[end of curve]);
	//void shear(t[0->1] , b[start of curve] , d[end of curve]);
	void updateData();
	void dumpDeltas();
	~Transform();
};

