#pragma once
#include "includes.h"
//#include "Sprite.h" //di lma kant hna kant 3amla error circular includes -> 7atetha fl cpp we ktabt hna extern class
//#include "GameObject.h" //di lma kant hna kant 3amla error circular includes -> 7atetha fl cpp we ktabt hna extern class

//typedef returnType (*FnPtr)(parameters);
typedef double(*FnPtr)(double);

struct motionData{
	CURVE animationName;
	MOTION affectedProperty;
	AXIS affectedAxis;
	float duration;
	unsigned long int starting_time;
	float curveStart_value;
	float curveEnd_value;
};

extern class GameObject;
extern class Sprite;
class motionLib
{
private:
	GameObject * mygameobject;

	//vector of running animations
	std::vector<motionData> motions; 
	motionData * waiting = NULL;


	void translate(int, float, AXIS axis);
	//void rotate(int, float);
	void scale(int, float, AXIS axis);

	void check(AXIS ax, MOTION pro, CURVE mot);
	
	bool contains(CURVE, AXIS, MOTION );

	// initialization: (for the string mapping of functions)
	std::map<CURVE, FnPtr> myFunctionsMap;
	std::map<MOTION, void(motionLib::*)(int, float, AXIS) > myAxisFunctionsMap; //map of member class function pointers that return void
public:
	void assignmotion(CURVE name, MOTION affectedProperty,AXIS affectedAxis, int duration, float curveStart, float curveEnd); //msh 3aref ana m7tag 7agat tania wla la2
	void stopmotion(int); 
	void updatemotions(); //this would cause problems as it forces the values, don't accumulate (mynf3sh 2 items yghyaro el scale msln , hy overwrite)

	

	motionLib(GameObject *);
	~motionLib();
};


typedef  void(motionLib::*axisFnPtr)(int, float, AXIS); //lazm ba3d el class
