#pragma once
#include "includes.h"
#include "Scene.h"

class Controller
{
public:
	virtual void leftMouseClick(glm::vec2 pos) = 0;
	virtual void rightMouseClick(glm::vec2 pos) = 0;
	virtual void hit(float vel, glm::vec2 pos) = 0;
	virtual void mouseMoved(glm::vec2 pos) = 0;
	virtual Scene * getScene() = 0;

	Controller();
	~Controller();
};

