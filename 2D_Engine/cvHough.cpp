#include "cvHough.h"

using namespace cv;
using namespace std;

bool compareContourAreas(std::vector<cv::Point> contour1, std::vector<cv::Point> contour2) {
	double i = fabs(contourArea(cv::Mat(contour1)));
	double j = fabs(contourArea(cv::Mat(contour2)));
	return (i < j);
}

cvHough::cvHough(EventSystem * e)
{
	this->ev = e;
	this->Initialize();
}


/// to CONNECT THE HITTING WITH THE GUI OF THE GAME
/// TODO in the cvFrameDiff class -->>> + the hit detecting function logic
void cvHough::sendHitToGUI(float vel, glm::vec2 pos){
	ev->simulateHit(vel, pos);
}


void cvHough::initializeCam(){
	cap = new VideoCapture(1);
	cap->set(CAP_PROP_FRAME_WIDTH, 320);
	cap->set(CAP_PROP_FRAME_HEIGHT, 240);
	cap->set(CAP_PROP_FPS, 120);

	if (!cap->isOpened()){
		cout << "Error opening video stream or file" << endl;
		exit(-1);
	}


	*cap >> frame;
	frame.copyTo(image);
	cvtColor(image, prevGray, COLOR_BGR2GRAY);
}

void cvHough::releaseCam(){
	cv::destroyAllWindows();
	if (cap)
		cap->release();
}

void cvHough::Initialize(){

	namedWindow("LK Demo", 1);
	//namedWindow("skin", 1);



	////////////////////////
	//@@@@@
	// ana hna 3aml 2 trackbar 3alashan agrb values param1,param2 btoo3 houghCircle .. the best was 30,40
	// el mafroud en l awel bta3 threshold canny wl tany acc ll circle .. kol mayzeed lazem  circle tb2a mazbota 3alashan ydetectaha 
	//////createTrackbar("1: ", "LK Demo", &thresh1, 255);
	//////createTrackbar("2: ", "LK Demo", &thresh2, 255);

	/*
	createTrackbar("ymin", "skin", &Y_MIN, 255);
	createTrackbar("ymax", "skin", &Y_MAX, 255);
	createTrackbar("crmin", "skin", &Cr_MIN, 255);
	createTrackbar("cr max", "skin", &Cr_MAX, 255);

	createTrackbar("cb min", "skin", &Cb_MIN, 255);
	createTrackbar("cv max", "skin", &Cb_MAX, 255);*/

	this->prev_clock = getTickCount();

	bgm = createBackgroundSubtractorMOG2(25);
	std::vector <cv::Point> dummy;
	std::vector <cv::Point> dummy2;
	sticks.push_back(dummy);
	sticks.push_back(dummy2);

	std::vector <hit_data> dummy3;
	std::vector <hit_data> dummy4;
	hits.push_back(dummy3);
	hits.push_back(dummy3);


	lazyNazumiHands_current.hand1.center = Point(0, 0);
	lazyNazumiHands_current.hand2.center = Point(0, 0);
	lazyNazumiHands_prev.hand1.center = Point(0, 0);
	lazyNazumiHands_prev.hand2.center = Point(0, 0);
}

void cvHough::help()
{
	cout << "\nHot keys: \n"
		"\tESC - quit the program\n"
		"\tr - auto-initialize tracking\n"
		"\tc - delete all the points\n"
		"\tn - switch the \"night\" mode on/off\n"
		"To add/remove a feature point click it\n" << endl;
}

void cvHough::lazyNazumi(){
	//current frame hand da yo3tbar el mouse dlwa2ty 
	// fa da mafrod y7arak el previous frame hand
	// we 3aiz atba3 el prv frame hand di fl curve we b3d kda ht3amel m3aha
	
	//dummy struct to carry the data
	Hands currentFrameHands;
	currentFrameHands.hand1.center = sticks[0][sticks[0].size() - 1];
	currentFrameHands.hand2.center = sticks[1][sticks[1].size() - 1];

	//smoothing factor
	float limitDistance = 10;

	//hand1
	float TotalDistance = sqrt(pow(lazyNazumiHands_current.hand1.center.y - currentFrameHands.hand1.center.y, 2) + pow(lazyNazumiHands_current.hand1.center.x - currentFrameHands.hand1.center.x, 2));
	if (TotalDistance > limitDistance){

		//lazyNazumiHands_prev = lazyNazumiHands_current;

		float xDelta, yDelta;
		xDelta = (currentFrameHands.hand1.center.x - lazyNazumiHands_current.hand1.center.x) * (TotalDistance - limitDistance) / TotalDistance;
		yDelta = (currentFrameHands.hand1.center.y - lazyNazumiHands_current.hand1.center.y) * (TotalDistance - limitDistance) / TotalDistance;
		lazyNazumiHands_current.hand1.center.x = lazyNazumiHands_current.hand1.center.x + xDelta;
		lazyNazumiHands_current.hand1.center.y = lazyNazumiHands_current.hand1.center.y + yDelta;
	}
	//hand2
	TotalDistance = sqrt(pow(lazyNazumiHands_current.hand2.center.y - currentFrameHands.hand2.center.y, 2) + pow(lazyNazumiHands_current.hand2.center.x - currentFrameHands.hand2.center.x, 2));
	if (TotalDistance > limitDistance){

		//lazyNazumiHands_prev = lazyNazumiHands_current;

		float xDelta, yDelta;
		xDelta = (currentFrameHands.hand2.center.x - lazyNazumiHands_current.hand2.center.x) * (TotalDistance - limitDistance) / TotalDistance;
		yDelta = (currentFrameHands.hand2.center.y - lazyNazumiHands_current.hand2.center.y) * (TotalDistance - limitDistance) / TotalDistance;
		lazyNazumiHands_current.hand2.center.x = lazyNazumiHands_current.hand2.center.x + xDelta;
		lazyNazumiHands_current.hand2.center.y = lazyNazumiHands_current.hand2.center.y + yDelta;
	}
	ev->setLeftStickPos(glm::vec2(lazyNazumiHands_current.hand1.center.x, lazyNazumiHands_current.hand1.center.y));
	ev->setRightStickPos(glm::vec2(lazyNazumiHands_current.hand2.center.x, lazyNazumiHands_current.hand2.center.y));

}

Mat cvHough::getSkin(Mat input)
{

	cv::Mat skin;
	//first convert our RGB image to YCrCb

	cv::cvtColor(input, skin, cv::COLOR_BGR2YCrCb);

	//uncomment the following line to see the image in YCrCb Color Space
	//cv::imshow("YCrCb Color Space",skin);

	//filter the image in YCrCb color space
	cv::inRange(skin, cv::Scalar(Y_MIN, Cr_MIN, Cb_MIN), cv::Scalar(Y_MAX, Cr_MAX, Cb_MAX), skin);

	//GaussianBlur(skin, skin,Size(3,3),1);

	//threshold(skin, skin, 1, 255, THRESH_BINARY);
	
	bitwise_not(skin, skin);

	//bgm->apply(skin, skin);

	//dilate(skin, skin, Mat());

	return skin;
}



void cvHough::doWork(){
	curr_clock = getTickCount();
	*cap >> frame;
	if (frame.empty())
		exit(-1);
	cv::flip(frame, frame, 1);
	frame.copyTo(image);
	frame.copyTo(finalOut);

	cvtColor(image, gray, COLOR_BGR2GRAY);

	frameCnt++;

	imageOut = Scalar::all(0);
	skinned = Scalar::all(0);
	imageWithoutSkin = Scalar::all(0);
	//@@@@@
	skinned = getSkin(image);
	//howa byget biha el soura menghir skin

	//threshold(skinned, skinned, 1, 255, THRESH_BINARY);
	image.copyTo(imageWithoutSkin, skinned);
	// hnapply MOG2 3la l image
	bgm->apply(image, imageOut);
	//output of mog tl3 f   imageout howa gray scale

	//@@@@@
	// out MOG2 bytla3 gray lvl .. fa b3mlo thresholding

	threshold(imageOut, imageOut, 1, 255, THRESH_BINARY);

	medianBlur(imageOut, imageOut, 7);

	erode(imageOut, imageOut, Mat());
	erode(imageOut, imageOut, Mat());
	erode(imageOut, imageOut, Mat());
	erode(imageOut, imageOut, Mat());



	dilate(imageOut, imageOut, Mat());
	dilate(imageOut, imageOut, Mat());
	dilate(imageOut, imageOut, Mat());
	dilate(imageOut, imageOut, Mat());
	dilate(imageOut, imageOut, Mat());


	erode(imageOut, imageOut, Mat());
	erode(imageOut, imageOut, Mat());
	erode(imageOut, imageOut, Mat());
	erode(imageOut, imageOut, Mat());


	dilate(imageOut, imageOut, Mat());
	dilate(imageOut, imageOut, Mat());
	dilate(imageOut, imageOut, Mat());

	erode(imageOut, imageOut, Mat());
	erode(imageOut, imageOut, Mat());

	dilate(imageOut, imageOut, Mat());
	dilate(imageOut, imageOut, Mat());

	image.copyTo(imageOut, imageOut);

	imageWithoutSkin2 = Scalar::all(0);
	skinned2 = getSkin(imageOut);
	//howa byget biha el soura menghir skin
	threshold(skinned2, skinned2, 1, 255, THRESH_BINARY);



	




	imageOut.copyTo(imageWithoutSkin2, skinned2);


	//imageWithoutSkin2 kda fiha soura el frame difference bkol el processing menghir  ay skin byt7rak aw sabt 
	//imshow("yyyyyyyyy", imageWithoutSkin2);
	imageWithoutSkin2.copyTo(imageOut);



	cvtColor(imageOut, imageGray, COLOR_BGR2GRAY);

	imageGray.copyTo(GrayforHough);

	// TO CONTOURS

	threshold(imageGray, imageGray, 1, 255, THRESH_BINARY);

	findContours(imageGray, contours, hierarchy, RETR_EXTERNAL, 2);


	for (int k = 0; k < contours.size(); k++){
		if (contourArea(contours[k]) < 20.00){
			contours.erase(contours.begin() + k);
			k--;
		}
		else{
			drawContours(imageOut, contours, k, Scalar(255,0,255), 2, 8, hierarchy, 0, Point());
		}

	}

	sort(contours.begin(), contours.end(), compareContourAreas);
	


	//Canny(imageGray, edges, thresh1, thresh2);
	cvtColor(imageOut, gray, COLOR_BGR2GRAY);


	HoughCircles(GrayforHough, circles, CV_HOUGH_GRADIENT, 1, 20, thresh1, thresh2, 1, 10);


	Mat estimated;
	Point statePt, measPt, ball[2];
	Point2f ballf[2];
	if ((sticks[0].size() > 1 && sticks[1].size()>1) && circles.size() >= 1 && contours.size())
	{


		Point stick1, stick2;	
		float radii;
		stick1.x = sticks[0][sticks[0].size() - 1].x;
		stick1.y = sticks[0][sticks[0].size() - 1].y;
		stick2.x = sticks[1][sticks[1].size() - 1].x;
		stick2.y = sticks[1][sticks[1].size() - 1].y;

		circle(finalOut, stick1, 3, Scalar(0, 255, 255), 3, 8, 0);
		circle(finalOut, stick2, 3, Scalar(0, 0, 255), 3, 8, 0);


		vector<vector<Point> > contours_poly(contours.size());
		vector<Moments> mu(contours.size());

		//Bassem : only assign it if it were near enough
		//edit: not here, down
		ball[0].x = circles[0][0];
		ball[0].y = circles[0][1];

		/*approxPolyDP(Mat(contours[0]), contours_poly[0], 3, true);
		minEnclosingCircle((Mat)contours_poly[0], ballf[0], radii);*/

		//mu[0] = moments(contours[0], false);
		//ball[0] = Point(mu[0].m10 / mu[0].m00, mu[0].m01 / mu[0].m00);


		
		if (contours.size() >= 2&&circles.size()>=2){

			
		
			ball[1].x = circles[1][0];
			ball[1].y = circles[1][1];
			
			/*approxPolyDP(Mat(contours[1]), contours_poly[1], 3, true);
			minEnclosingCircle((Mat)contours_poly[1],	ballf[1], radii);*/

			/*mu[1] = moments(contours[1], false);
			ball[1] = Point(mu[1].m10 / mu[1].m00, mu[1].m01 / mu[1].m00);*/


			float distancefirststicktoPoint1 = getDistance(ball[0], stick1);
			float distancefirststicktoPoint2 = getDistance(ball[1], stick1);
			
			float distancesecondsticktoPoint1 = getDistance(ball[0], stick2);
			float distancesecondsticktoPoint2 = getDistance(ball[1], stick2);


			//clear the duplicate circles!
			for (int i = 0; i < circles.size(); i++){
				for (int j = i+1; j < circles.size(); j++){
					if (getDistance(Point(circles[i][0], circles[i][1]), Point(circles[j][0], circles[j][1])) < 10){
						circles.erase(circles.begin() + j);
						i--;
						j--;
					}
				}
			}

			
			if (circles.size() < 2)
				assert(0);


			if (distancefirststicktoPoint1 < distancesecondsticktoPoint1)
				sticks[0].push_back(ball[0]);
			else
				sticks[1].push_back(ball[0]);

			if (distancefirststicktoPoint2 < distancesecondsticktoPoint2)
				sticks[0].push_back(ball[1]);
			else
				sticks[1].push_back(ball[1]);

			//if (distancefirststicktoPoint1<distancefirststicktoPoint2)
			//{
			//	// stick 1 >> point 1;
			//	//?? stick 2 >>2 ;
			//	if (distancesecondsticktoPoint2 < distancesecondsticktoPoint1){
			//		sticks[0].push_back(ball[0]);
			//		sticks[1].push_back(ball[1]);
			//	}
			//	else{
			//		// stick 2 >> 1 brdo  ;
			//		if (distancefirststicktoPoint1 < distancesecondsticktoPoint1){
			//			// stick 1 >> 1 ;
			//			sticks[0].push_back(ball[0]);
			//			sticks[1].push_back(ball[1]);
			//		}
			//		else{
			//			sticks[0].push_back(ball[1]);
			//			sticks[1].push_back(ball[0]);

			//		}

			//	}


			//}





			////////////////////////////
			////if ((distancefirststicktoPoint1 < distancesecondsticktoPoint1))
			////{

			////	sticks[0].push_back(ball[0]);
			////	sticks[1].push_back(ball[1]);

			////}
			////else
			////{
			////	sticks[0].push_back(ball[1]);
			////	sticks[1].push_back(ball[0]);
			////}

		}
		else{

			// test bassem
			//sticks[0].push_back(ball[0]);
			//sticks[1].push_back(ball[0]);
			cout << "error here?" << endl;
			
			//assuming en el circles mfish gher wa7da el sa7 3ashan mfish gher strong circles wa7da
			//bs min 2al hya ani stick ? -> hya di l moshkla !!!!
			/*for (int i = 0; i < circles.size(); i++){
				if (getDistance(Point(circles[i][0], circles[i][1]), sticks[0].back()) < leastDistoStick)
					dump = Point(circles[i][0], circles[i][1]);
			}*/
			float distanceStick1, distanceStick2;
			distanceStick1 = getDistance(sticks[0].back(), ball[0]);
			distanceStick2 = getDistance(sticks[1].back(), ball[0]);
			if (distanceStick1<distanceStick2)
				sticks[0].push_back(ball[0]);
			else
				sticks[1].push_back(ball[0]);


			/*sticks[0].push_back(sticks[0].back());
			sticks[1].push_back(ball[0]);*/
			//Bassem EDIT
			//for (int i = 0; i < circles.size(); i++)
			//	if (getDistance(Point(circles[i][0], circles[i][1]), sticks[1].back()) < 30)
			//		sticks[1].push_back(ball[0]);


		}






		line(finalOut, sticks[0][sticks[0].size() - 1], sticks[0][sticks[0].size() - 2], Scalar(255, 255, 255));
		line(finalOut, sticks[1][sticks[1].size() - 1], sticks[1][sticks[1].size() - 2], Scalar(255, 255, 255));

	}
	else
	{

		if (sticks[0].size() && sticks[1].size()){

			sticks[0].push_back(sticks[0][sticks[0].size() - 1]);
			sticks[1].push_back(sticks[1][sticks[1].size() - 1]);

		}
		else{
			sticks[0].push_back(Point(20, 20));
			sticks[1].push_back(Point(200, 20));
		}


	}


	// deh lly ttrsm
	circle(finalOut, sticks[0][sticks[0].size() - 1], 10, Scalar(0, 255, 255), 3, 8, 0);
	circle(finalOut, sticks[1][sticks[1].size() - 1], 10, Scalar(0, 0, 255), 3, 8, 0);



	//cout<<"number of cont : " << contours.size() << "  ";





	/*circle(finalOut, Point(ball[0]), 10, Scalar(0, 255, 255), 3, 8, 0);
	circle(finalOut, Point(ball[1]), 10, Scalar(0, 0, 255), 3, 8, 0);*/

	lazyNazumi();
	circle(finalOut, lazyNazumiHands_current.hand1.center, 3, Scalar(255, 0, 0), 3, 8, 0);
	circle(finalOut, lazyNazumiHands_current.hand2.center, 3, Scalar(255, 0, 0), 3, 8, 0);
	calc_velocity();
	
	lazyNazumiHands_prev = lazyNazumiHands_current;
	
	//circle(finalOut, predictPt, 3, Scalar(0, 255, 0), -1, 8, 0);
	// circle outline
	//circle(image, center, radius, Scalar(255, 0, 0), 3, 8, 0);
	//circle(finalOut, predictPt, 10, Scalar(0, 0, 255), 3, 8, 0);

	//resize(finalOut, finalOut, Size(640, 480));
	imshow("LK Demo", finalOut);
	//imshow("image", imageOut);
	//imshow("skin", getSkin(image));

	// 
	//if (waitKey(0) >= 0) continue;
	//char c = (char)waitKey(1);
	//if (c == 27)
	//	exit(0);



	cv::swap(prevGray, gray);
	cv::swap(prevGrayState, grayState);
	circles.clear();
	reduce_vector(hits[0]);
	reduce_vector(hits[1]);
	prev_clock = curr_clock;

	//char cc = (char)waitKey(1);
	//if (cc == 27)
	//	return;


	//if (waitKey(1) > 0)
	//	return;

}

void cvHough::reduce_vector(vector<Point> &v){
	if (v.size()>100){
		v.erase(v.begin(), v.begin() + 50);
	}
}

void cvHough::reduce_vector(vector<hit_data> &v){
	if (v.size()>100){
		v.erase(v.begin(), v.begin() + 50);
	}
}

void cvHough::calc_velocity()
{
	long double time = (curr_clock - prev_clock) / getTickFrequency();
	//cout << "================ " << time << " ======\n";
	second += time;
	bassemFrameCounter++;

	if (second >= 1000){
		second = 0;
		//cout << "FPS: " << bassemFrameCounter << endl;
		bassemFrameCounter = 0;
	}

	long double Vx, Vy, V1, V2, acc;
	hit_data temp;
	Point prev, curr;
	for (int i = 0; i < 2; i++){
		if (sticks[i].size() >= 2)
		{


			prev = sticks[i][sticks[i].size() - 2];
			curr = sticks[i][sticks[i].size() - 1];
			
			////Bassem: for lazy nazumy function (to use its output)
			//if (i == 0) {
			//	curr = lazyNazumiHands_current.hand1.center;
			//	prev = lazyNazumiHands_prev.hand1.center;
			//}
			//else {
			//	curr = lazyNazumiHands_current.hand2.center;
			//	prev = lazyNazumiHands_prev.hand2.center;
			//}
			////////////////////////////////////////
			Vx = (curr.x - prev.x) / time;
			Vy = (curr.y - prev.y) / time;

			temp.p = curr;
			temp.Vx = Vx;
			temp.Vy = Vy;
			temp.accel = 0.0;

			//cout << "STICK: " << i << " Vx = " << Vx << " Vy = " << Vy << " " ;

			hits[i].push_back(temp);

			//cout << "--STICK: " << " 1 = " << hits[0][hits[0].size() - 1].Vy << " 2 = " << hits[1][hits[1].size() - 1].Vy << endl;
			if (hits[i].size() >= 2){

				V1 = sqrt((hits[i][hits[i].size() - 1].Vx)*(hits[i][hits[i].size() - 1].Vx) + (hits[i][hits[i].size() - 1].Vy)*(hits[i][hits[i].size() - 1].Vy));
				V2 = sqrt((hits[i][hits[i].size() - 2].Vx)*(hits[i][hits[i].size() - 2].Vx) + (hits[i][hits[i].size() - 2].Vy)*(hits[i][hits[i].size() - 2].Vy));

				acc = (V1 - V2) / time;
				hits[i][hits[i].size() - 1].accel = acc;
			}

			//cout << endl;

		}
		else{

			curr = sticks[i][sticks[i].size() - 1];
			////Bassem: for lazy nazumy function (to use its output)
			//if (i == 0) {
			//	curr = lazyNazumiHands_current.hand1.center;
			//}
			//else {
			//	curr = lazyNazumiHands_current.hand2.center;
			//}
			/////////////////////////////////////////
			temp.p = curr;
			temp.Vx = 0.0;
			temp.Vy = 0.0;
			temp.accel = 0.0;
			hits[i].push_back(temp);

		}
	}

	//hena x we y bn3mlhom set fl EventSystem
	//ev->setLeftStickPos(glm::vec2(sticks[0][sticks[0].size() - 1].x, sticks[0][sticks[0].size() - 1].y));
	ev->setLeftStickPos(glm::vec2(lazyNazumiHands_current.hand1.center.x, lazyNazumiHands_current.hand1.center.y));
	//ev->setRightStickPos(glm::vec2(sticks[1][sticks[1].size() - 1].x, sticks[1][sticks[1].size() - 1].y));
	ev->setRightStickPos(glm::vec2(lazyNazumiHands_current.hand2.center.x, lazyNazumiHands_current.hand2.center.y));
	detectHit();
}

void cvHough::detectHit(){
	//	cout << "STICK: " << " 1 = " << hits[0][hits.size() - 1].Vy << " 2 = " << hits[1][hits.size() - 1].Vy << endl;
	if (hits[0].size() > 10 && hits[1].size() > 10){


		if ((hits[0][hits[0].size() - 5].Vy >0.0 && hits[0][hits[0].size() - 4].Vy >0.0 && hits[0][hits[0].size() - 3].Vy >0.0 &&  hits[0][hits[0].size() - 2].Vy <= 20 && hits[0][hits[0].size() - 1].Vy <= 0.0)){


			line(finalOut, Point(hits[0][hits[0].size() - 1].p.x - 50, hits[0][hits[0].size() - 1].p.y), Point(hits[0][hits[0].size() - 1].p.x + 50, hits[0][hits[0].size() - 1].p.y), Scalar(255, 255, 255), 3);
			cnt_hit1++;
			//cout << "**HIT  (0): " << cnt_hit1 << endl;

			//x = hits[0][hits[0].size() - 1].p
			//power =  hits[0][hits[0].size() - 1].accel*
			//here origin is at top left , which is incompatible with the GUI (origin and bottom left)
			this->sendHitToGUI(hits[0][hits[0].size() - 1].accel, glm::vec2(hits[0][hits[0].size() - 1].p.x, hits[0][hits[0].size() - 1].p.y));
		}

		if ((hits[1][hits[1].size() - 5].Vy >0.0 &&hits[1][hits[1].size() - 4].Vy >0.0 &&  hits[1][hits[1].size() - 2].Vy <= 20 && hits[1][hits[1].size() - 1].Vy <= 0.0)){

			line(finalOut, Point(hits[1][hits[1].size() - 1].p.x - 50, hits[1][hits[1].size() - 1].p.y), Point(hits[1][hits[1].size() - 1].p.x + 50, hits[1][hits[1].size() - 1].p.y), Scalar(255, 255, 255), 3);
			cnt_hit2++;
			//cout << "**HIT  (1): " << cnt_hit2 << endl;

			this->sendHitToGUI(hits[1][hits[1].size() - 1].accel, glm::vec2(hits[1][hits[1].size() - 1].p.x, hits[1][hits[1].size() - 1].p.y));

		}


	}
}


float cvHough::getDistance(Point p1, Point p2){

	return sqrt(pow(abs(p1.x - p2.x), 2) + pow(abs(p1.y - p2.y), 2));
}


cvHough::~cvHough()
{
}
