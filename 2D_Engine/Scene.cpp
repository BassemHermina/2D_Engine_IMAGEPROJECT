#include "Scene.h"

Scene::Scene()
{

}

void Scene::addGameobject(GameObject * g){
	this->gameobjects.push_back(g);
}

//updates the data of the GameObject.Transform according to the motionLibrary and the Update Function (if it has one)
//and sets these info inside the Transform per step
void Scene::updatedata(){
	for (int i = 0; i < gameobjects.size(); i++){
		gameobjects[i]->motionlib.updatemotions();  //adds the motion data to frameDeltas of the gameobject
		//gameobjects[i]->transform.updateData(); //sums the frameDelta and RefPosition into Position (to be used to acquire the position by user)
		// updateData is called after every operation inside Transform, that updates any data inside it
	}

	for (int i = 0; i < gameobjects.size(); i++){
		if (gameobjects[i]->Update != NULL) 
			gameobjects[i]->Update(gameobjects[i]); 
		//gameobjects[i]->transform.updateData();
	}
}

void Scene::render(){
	// Clear the screen
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //if no bg
	//glClear(GL_DEPTH_BUFFER_BIT); //with bg
	//trtib l render bl sha2loob
	for (int i = 0; i < gameobjects.size(); i++){
		gameobjects[i]->transform.updateData();
		gameobjects[i]->render();
	}
}

void Scene::dumpGameobjectsDeltas(){
	//-- > render loop (last thing to do)
	for (int i = 0; i < gameobjects.size(); i++){
		gameobjects[i]->transform.dumpDeltas();
	}
	
}


Scene::~Scene()
{
}
