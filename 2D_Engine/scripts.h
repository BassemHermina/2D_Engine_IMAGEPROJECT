#include "includes.h"
#include "Sprite.h"
#include "GameObject.h"
#include "EventSystem.h"

int zz = 0;
bool mode1 = false;
bool mode2 = false;
bool once = true;

void Sprite1Start(GameObject * me){
	printf("Start Called! %d \n",zz);
	//me->transform.setRefPosition(glm::vec2(100, 100));
	EventSystem * events = EventSystem::getEventSystem();

}

void Sprite1Update(GameObject * me){
	me->transform.setRefPosition(glm::vec2(me->transform.RefPos.x + 0.005, me->transform.RefPos.y));
	
}

void handleFollowMouse(GameObject * me){
	EventSystem * events = EventSystem::getEventSystem();
	me->transform.setRefPosition(events->getMouseLocation());
}

void handleFollowLeftStick(GameObject * me){
	EventSystem * events = EventSystem::getEventSystem();
	me->transform.setRefPosition(events->getLeftStickLocation());
}

void handleFollowRightStick(GameObject * me){
	EventSystem * events = EventSystem::getEventSystem();
	me->transform.setRefPosition(events->getRightStickLocation());
}

void selectHandsMode(){
	mode1 = true;
}

void selectSticksMode(){
	mode2 = true;
}

void exitMode(){
	mode1 = false;
	mode2 = false;
}